<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/contact', function () {
    return view('contact');
});

Route::get('/mentions-legales', function () {
    return view('terms');
});

Route::get('/cgu', function () {
    return view('cgu');
});

Route::get('/historique-avis', function () {
    return view('reviews_history_shop');
});

Route::get('/details-promo', function () {
    return view('promo_details');
});

Route::get('/stats', function () {
    return view('dashboard.stats');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

/*Map*/
Route::post('/city', array('as'=>'city', 'uses'=>'MapController@getCities'));
Route::get('/marker', 'MapController@getShopWithPromoActives')->name('marker');
Route::get('/type', 'MapController@getTypesCategories')->name('typeCat');

Route::get('/user/edit/', 'CompteController@getEdit')->name('user_edit');

Route::post('/user/edit', 'CompteController@postEdit')->name('user_edit_post');

Route::get('/resp/shop/{id_shop}', 'ShopController@shop')->name('shop');
Route::get('/resp/shops', 'ShopController@getShops')->name('resp_shops');

//Route::get ('/resp/shop/{id_shop}/edit', 'ShopController@editShop')->name('edit_shop');
Route::post ('/resp/shop/{id_shop}/edit', 'ShopController@editShopPost')->name('edit_shop_post');

Route::get('/resp/shops/add', 'ShopController@addShop')->name('add_shop');
Route::post('/resp/shops/add', 'ShopController@addShopPost')->name('add_shop_post');

Route::post('/resp/promos/add', 'PromoController@addPromoPost')->name('add_promo_post');

Route::get('login/facebook', 'Auth\LoginController@redirectToProvider');
Route::get('login/facebook/callback', 'Auth\LoginController@handleProviderCallback');

Route::get('promotion/{id_promo}', 'PromoController@getPromo')->name('get_promo');

Route::get('/resp/promo/{id_promo}/edit', 'PromoController@editPromo')->name('edit_promo');
Route::post('/resp/promo/{id_promo}/edit', 'PromoController@editPromoPost')->name('edit_promo_post');

Route::post('/user/edit/password', 'CompteController@passwordEdit')->name('password_edit');

Route::get('promotion/adhesion/{id_promo}', 'AdhesionController@adhesion')->name('adhesion_promo');
Route::post('promotion/adhesion/{id_promo}/comment', 'AdhesionController@commentaire_post')->name('adhesion_commentaire_post');

Route::get('/home', 'AdhesionController@getAdhesion')->name('adhesion_list');
Route::get('/home/delete/{id_adhesion}', 'AdhesionController@deleteAdhesion')->name('delete_adhesion');



/* Admin */
Route::get('/admin', 'AdminController@dashboard')->name('admin');
Route::post('/admin/type/add', 'AdminController@addTypePost')->name('add_type_post');
Route::get('/admin/type/{id_type}', 'AdminController@displayType')->name('display_type');
Route::post('/admin/type/{id_type}/addcat', 'AdminController@addCatPost')->name('add_cat_post');
Route::get('/admin/type/{id_type}/edit', 'AdminController@editType')->name('edit_type');
Route::post('/admin/type/{id_type}/edit', 'AdminController@editTypePost')->name('edit_type_post');