<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'Dismap') }}</title>

        <link rel="icon" type="image/png" href="{{ asset('img/logo_s.png') }}">

        <!-- Styles -->
        <link href="{{ asset('bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
        <link rel="stylesheet" href="{{ asset('css/fontawesome.min.css') }}">
        @if (Request::path() == '/')
            <link rel="stylesheet" href="{{ asset('css/leaflet.css') }}">
        @elseif(Request::path() == ('user/edit' || is('resp/*')))
            <link rel="stylesheet" href="{{ asset('css/datatables.min.css') }}">
        @endif
        <link href="{{ asset('css/main.css') }}" rel="stylesheet">
    </head>

    <body>
        @yield('content')
    </body>
</html>
