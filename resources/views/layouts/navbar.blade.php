<nav class="navbar navbar-expand-lg navbar-light bg-white shadow-sm">
    <div class="container">
        <a class="navbar-brand" href="{{ url('/') }}">
            <img src="{{ asset('img/logo_w-nom.svg') }}" alt="Dismap logo" style="height: 40px;">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar" aria-controls="navbar" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbar">
            <ul class="navbar-nav ml-auto d-flex align-items-center font-weight-bolder">
                <li class="nav-item active">
                    <a class="nav-link" href="{{ url('/') }}">Accueil <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ url('contact') }}">Contact</a>
                </li>

                @guest
                    <li class="nav-item my-2 ml-lg-3">
                        <a class="btn btn-outline-primary font-weight-bold btn-pill" href="{{ route('login') }}">Connexion</a>
                    </li>
                @endguest

                @auth()
                    @if (Auth::user()->type == "responsable")
                        <li class="nav-item">
                            <a class="nav-link" href="{{ url('home') }}">Dashboard</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('resp_shops') }}">Gestion magasins</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ url('stats') }}">Statistiques</a>
                        </li>
                    @else
                        <li class="nav-item">
                            <a class="nav-link" href="{{ url('home') }}">Dashboard</a>
                        </li>
                    @endif
                    <li class="nav-item dropdown">
                        <a class="btn btn-primary btn-pill dropdown-toggle font-weight-bold text-capitalize ml-md-3" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            {{ Auth::user()->firstname}} {{ Auth::user()->lastname }}
                        </a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="{{ route('user_edit') }}">Mon compte</a>
                            <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                    {{ csrf_field() }}
                                </form>
                                Déconnexion
                            </a>
                        </div>
                    </li>
                @endauth
            </ul>
        </div>
    </div>
</nav>
