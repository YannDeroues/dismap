<footer class="footer--container">
    <div class="offset-md-1 col-md-10 py-5">
        <div class="row">
            {{-- Réseaux Sociaux --}}
            <div class="col-12 col-lg py-3">

                <h4 class="text-uppercase mb-4 font-weight-bold">Nos réseaux sociaux</h4>

                <div class="row">
                    <div class="col">
                        <div class="border-top my-3 border-white"></div>
                    </div>
                    <div class="col">
                        <a href="#" class="h3 text-decoration-none text-muted">
                            <i class="fab fa-facebook-square"></i>
                        </a>
                    </div>
                    <div class="col">
                        <a href="#" class="h3 text-decoration-none text-muted">
                            <i class="fab fa-twitter"></i>
                        </a>
                    </div>
                    <div class="col">
                        <a href="#" class="h3 text-decoration-none text-muted">
                            <i class="fab fa-instagram"></i>
                        </a>
                    </div>
                    <div class="col">
                        <a href="#" class="h3 text-decoration-none text-muted">
                            <i class="fab fa-discord"></i>
                        </a>
                    </div>
                    <div class="col">
                        <a href="#" class="h3 text-decoration-none text-muted">
                            <i class="fab fa-snapchat-ghost"></i>
                        </a>
                    </div>
                </div>
            </div>

            {{-- Questions --}}
            <div class="col-12 col-lg py-3">
                <h4 class="font-weight-bold">Vous avez une question ?</h4>
                <p class="mb-3 font-weight-bolder text-muted">Vous souhaitez faire une demande de partenariat ? Ou juste nous faire une remarque ?</p>
                <a href="{{ url('contact') }}" class="btn btn-primary btn-pill font-weight-bold">Contactez-nous !</a>
            </div>

            {{-- Partenaires --}}
            <div class="col col-lg py-3">
                <h4 class="text-uppercase font-weight-bold">Nos Partenaires</h4>
                <div class="row align-items-center">
                    <div class="col">
                        <a href="#" target="_blank" rel="nofollow">
                            <img src="{{ asset('img/partners/lazyload.png') }}" data-src="{{ asset('img/partners/default.png') }}" alt="Exemple de logo partenaire" class="img-fluid lazyload shadow-sm">
                        </a>
                    </div>
                    <div class="col">
                        <a href="#" target="_blank" rel="nofollow">
                            <img src="{{ asset('img/partners/lazyload.png') }}" data-src="{{ asset('img/partners/default.png') }}" alt="Exemple de logo partenaire" class="img-fluid lazyload shadow-sm">
                        </a>
                    </div>
                    <div class="col">
                        <a href="#" target="_blank" rel="nofollow">
                            <img src="{{ asset('img/partners/lazyload.png') }}" data-src="{{ asset('img/partners/default.png') }}" alt="Exemple de logo partenaire" class="img-fluid lazyload shadow-sm">
                        </a>
                    </div>
                    <div class="col">
                        <a href="#" target="_blank" rel="nofollow">
                            <img src="{{ asset('img/partners/lazyload.png') }}" data-src="{{ asset('img/partners/default.png') }}" alt="Exemple de logo partenaire" class="img-fluid lazyload shadow-sm">
                        </a>
                    </div>
                    <div class="col">
                        <a href="#" target="_blank" rel="nofollow">
                            <img src="{{ asset('img/partners/lazyload.png') }}" data-src="{{ asset('img/partners/default.png') }}" alt="Exemple de logo partenaire" class="img-fluid lazyload shadow-sm">
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid copyright">
        <div class="row">
            <div class="col-12 col-md-4">
                <p>DISMAP | Copyright © {{ date('Y') }} - Tous droits réservés</p>
            </div>
            <div class="col col-md-4 pb-4 py-md-0 offset-md-4 d-flex align-items-center justify-content-md-end">
                <div>
                    <a href="{{ url('cgu') }}">CGU</a> | <a href="{{ url('mentions-legales') }}">Mentions légales</a>
                </div>
            </div>
        </div>
    </div>
</footer>