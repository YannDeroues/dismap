<!-- Scripts -->
<script src="{{ asset('js/jquery.min.js') }}"></script>
<script src="{{ asset('js/popper.min.js') }}"></script>
<script src="{{ asset('bootstrap/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/shards.min.js') }}"></script>
<script src="{{ asset('js/lazyload.min.js') }}"></script>
@if (Request::path() == ('user/edit' || is('resp/*')))
    <script src="{{ asset('js/index.js') }}"></script>
@endif
<script>lazyload();</script>
