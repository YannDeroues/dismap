@extends('layouts.app')

@section('content')

    @include('layouts.navbar')

    <div id="carouselExampleControls" class="carousel slide carousel-fade" data-ride="carousel">
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img src="{{ asset('img/slides/lazyload.jpeg') }}" data-src="{{ asset('img/slides/intro-1.jpeg') }}" class="d-block lazyload" alt="Illutration 1">
            </div>
            <div class="carousel-item">
                <img src="{{ asset('img/slides/lazyload.jpeg') }}" data-src="{{ asset('img/slides/intro-3.jpeg') }}" class="d-block lazyload" alt="Illutration 2">
            </div>
            <div class="carousel-item">
                <img src="{{ asset('img/slides/lazyload.jpeg') }}" data-src="{{ asset('img/slides/intro-3.jpeg') }}" class="d-block lazyload" alt="Illutration 3">
            </div>
        </div>
    </div>

	<div class="searchBar--box">
	    <div class="container-fluid">
            <div class="row">
                <div class="offset-md-2 col-md-8 searchBar--box-title">
                    <h1 class="display-4 font-weight-bold text-white">Des promos toute l'année !</h1>
                </div>
            </div>
            <div class="row">
                <div class="offset-md-2 col-md-8 py-4 py-md-2 searchBar--box-container shadow">
                    <div class="row">
                        <div class="col-md form-group mb-md-0 d-flex align-items-center">
                            <input id="villes" list="villeDatalist" type="text" class="form-control border-0 shadow-none font-weight-bold" placeholder="Entrer une ville">
                            <datalist id="villeDatalist"></datalist>
                        </div>
                        <div class="col-md form-group mb-md-0">
                            <select id="selectType" class="selectpicker border-0 shadow-none"></select>
                        </div>
                        <div class="col-md form-group mb-md-0 no-divider">
                            <select id="selectCat" class="selectpicker border-0 shadow-none cat"></select>
                        </div>
                    </div>
                </div>
            </div>
	    </div>
	</div>

    <div class="container-fluid">
        <div class="row">
            <div class="offset-md-1 col-md-10 searchBar--box-map">
                <div id="map" class="shadow"></div>
            </div>
        </div>
    </div>

    @include('layouts.footer')

    @include('layouts._js')
    <script src="{{ asset('js/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('js/leaflet.js') }}"></script>
    <script src="{{ asset('js/axios.min.js') }}"></script>

    <script>
        /* VARIABLES */
        /*Pour leaflet*/
        let map = L.map('map');
        L.control.scale().addTo(map);

        /*Pour la géolocalisation*/
        let options = {
            enableHighAccuracy: true,
            timeout: 5000,
            maximumAge: 0
        };

        let osmUrl='http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
        let osmAttrib='© OpenStreetMap';
        let osm = new L.TileLayer(osmUrl, {
            attribution: osmAttrib,
            minZoom: 4
        });
        map.setView([47.0, 3.0], 6);
        map.addLayer(osm);

        let input = document.getElementById('villes');
        let datalist = document.getElementById('villeDatalist');
        let selectType = document.getElementById('selectType');
        let selectCat = document.getElementById('selectCat');

        /*Stockage de la requête sql précédente*/
        let memoryReq;

        /*Pour l'affichage des promo*/
        let marker;
        let markersLayer = new L.LayerGroup();

        /*Variables contenant des fonctions*/
        let errorCity = () =>{return console.log('pb ville')};
        let error = () =>{return console.log('Il semble qu\'il y ait un problème')};

        let findCityDone = (r) =>{
            let rep = r.data;
            let nameCity = input.value.split(' | ');

            /*On suprime le contenu de la datalist*/
            datalist.innerHTML="";

            /*On actualise memoryReq que si rep retourne un résultat*/
            if (rep['cities'].length !== 0) {memoryReq = rep['cities'];}

            /*Si la requête sql (rep['cities']) est vide, on vérifie dans memoryReq (qui est le stockage de la requête sql précédente) sinon on rempli la datalist avec la rep*/
            if(rep['cities'].length == 0){
                for (let i=0; i<memoryReq.length; i++){
                    if ((nameCity[0].toUpperCase() == memoryReq[i].nom_ville.toUpperCase()) && (nameCity[1] == memoryReq[i].cp_ville)) {
                        map.setView([memoryReq[i].lat_ville, memoryReq[i].long_ville], 15);
                    }
                }
            } else {
                for (let i=0; i<rep['cities'].length; i++){
                    /*Si le nom dans l'input correspond au nom d'une ville, on recentre la carte sur la ville*/
                    if (nameCity[0].toUpperCase() == rep['cities'][i].nom_ville.toUpperCase()) {
                        map.setView([rep['cities'][i].lat_ville, rep['cities'][i].long_ville], 15);
                    }

                    let option = datalist.appendChild(document.createElement('option'));
                    option.setAttribute('value', rep['cities'][i].nom_ville+' | '+rep['cities'][i].cp_ville);
                }
            }
        };

        let findStructureDone = (r) =>{
            let rep = r.data;
            markersLayer.clearLayers();

            for (let i = 0; i < rep.length; i++) {
                marker = L.marker([rep[i].lat_mag, rep[i].long_mag]);

                marker.bindPopup(()=>{return '<a href="#" title="">'+rep[i].libelle_promo+'</a><br/>'+rep[i].nom_mag});

                let aPromo = '<a href="/promotion/'+rep[i].id+'" target="_blank">Voir la promo</a>';
                marker.bindPopup(() => {
                    return '<p class="font-weight-bold m-0">' +
                        rep[i].libelle_promo +
                        '</p><p class="m-0">' +
                        rep[i].nom_mag + '</p>' +
                        aPromo;
                });
                markersLayer.addLayer(marker);
                /*let titlePromo = divPromo.appendChild(document.createElement('h2'));
                titlePromo.innerText = rep[i].libelle_promo;*/
            }
            markersLayer.addTo(map);
        };

        /* FONCTIONS */


        /* Lance la requête ajax pour rechercher les villes */
        function findCity(inputValue){
            axios.post(
                '{{route('city')}}', 
                {inputValue:inputValue, 
                    _token: '{{ csrf_token() }}'
                }
            )
            .then(findCityDone)
            .catch(errorCity); 
        }

        /*Teste la valeur de l'input et lance la requête ajax*/
        function startFindCity(){
            if(input.value.length > 2){
                findCity(input.value);
            } else if (datalist.childElementCount) {
                datalist.innerHTML="";
            }
        }

        function findStructure(type, cat){
            /* Lance la requête ajax pour rechercher les magasins dans la zone de la carte */
            let idType;
            let idCategorie;
            if (findStructure.arguments.length == 1){
                idType = selectType.options[selectType.selectedIndex].value;
            } else if(findStructure.arguments.length == 2){
                idType = selectType.options[selectType.selectedIndex].value;
                idCategorie = selectCat.options[selectType.selectedIndex].value;
            }
            let latMapMin = map.getBounds().getSouthWest().lat;
            let latMapMax = map.getBounds().getNorthEast().lat;
            let longMapMin = map.getBounds().getSouthWest().lng;
            let longMapMax = map.getBounds().getNorthEast().lng;
            axios.get(
                '{{route('marker')}}', {
                    params : {
                        latMapMax:latMapMax, 
                        latMapMin:latMapMin, 
                        longMapMax:longMapMax, 
                        longMapMin:longMapMin,
                        idType:idType,
                        idCategorie:idCategorie
                    }
                }
            )
            .then(findStructureDone)
            .catch(error);
        }

        /* Teste le niveau de zoom de la map, affiche les promo actives ou les  supprime */
        function startFindPromo(){
            if (map.getZoom()>11) {

                findStructure();
            } else {
                markersLayer.clearLayers();
            }
        }

        function displayTypes() {
            axios.get('{{route('typeCat')}}')
                .then((r) => {
                    let types = r.data.types;
                    let categories = r.data.categories;
                    let parDefaut = selectType.appendChild(document.createElement('option'));
                    parDefaut.setAttribute('selected', '');
                    parDefaut.textContent = "Choisissez un type";
                    let parDefautCat = selectCat.appendChild(document.createElement('option'));
                    parDefautCat.setAttribute('selected', '');
                    parDefautCat.textContent = "Choisissez une catégorie";
                    for (let i = 0; i < types.length; i++) {
                        let option = selectType.appendChild(document.createElement('option'));
                        option.setAttribute('value', types[i].id);
                        option.textContent = types[i].libelle_type;
                    }
                    selectType.addEventListener('change', ()=>{
                        let idTypeSelect = selectType.options[selectType.selectedIndex].value;
                        if (idTypeSelect == "Choisissez un type") {
                            findStructure();
                        }
                        for (let i = 0; i < categories.length; i++) {
                            if (idTypeSelect == categories[i].id_type) {
                                let option = selectCat.appendChild(document.createElement('option'));
                                option.setAttribute('value', categories[i].id);
                                option.appendChild(document.createTextNode(categories[i].libelle_cat));
                            } else {
                                $('.cat').find('[value='+categories[i].id+']').remove();
                            }
                        }
                        $('.selectpicker').selectpicker('refresh');
                        findStructure(idTypeSelect);
                    });
                    selectCat.addEventListener('change', ()=>{
                        let valueSelect = selectType.options[selectType.selectedIndex].value;
                        let idCatSelect = selectCat.options[selectCat.selectedIndex].value;
                        console.log(idCatSelect == 'Choisissez une catégorie');
                        if (idCatSelect == 'Choisissez une catégorie'){
                            findStructure(valueSelect);
                        } else {
                            findStructure(valueSelect, idCatSelect);
                        }
                    });

                })
                .catch(error);
        }


        /*Géolocalisation*/
        function successGeo(pos) {
            let crd = pos.coords;
            map.setView([crd.latitude, crd.longitude], 15);
        }

        function errorGeo(err) {
            console.warn(`ERREUR (${err.code}): ${err.message}`);
        }

        /*Code à exécuter*/
        document.addEventListener('DOMContentLoaded', function(){

            navigator.geolocation.getCurrentPosition(successGeo, errorGeo, options);

            displayTypes();

            /*On ajoute un évènement startFindCity sur l'input quand on tape sur le clavier*/
            input.addEventListener('keyup', function(){startFindCity();});
            /*On ajoute le même évènement sur la datalist au clic*/
            datalist.addEventListener('click', function(){startFindCity();});

            /*On ajoute le même évènement sur la datalist quand on appuie sur une touche*/
            datalist.addEventListener('keyup', function(){startFindCity();});

            map.on('moveend', function(){
                startFindPromo();
            });

        });
    </script>
@endsection
