@extends('layouts.app')

@section('content')

    @include('layouts.navbar')

    <div class="bg-light py-5">
        <h1 class="display-4 text-center text-uppercase py-5">Mentions légales</h1>
    </div>

    <div class="container mt-5">
        <div class="row">
            <div class="col-12 col-md-3 mb-5">
                <nav class="nav flex-column sticky">
                    <a class="nav-link" href="#1">1. Présentation du site</a>
                    <a class="nav-link" href="#2">2. Conditions générales d’utilisation du site et des services proposés.</a>
                    <a class="nav-link" href="#3">3. Description des services fournis</a>
                    <a class="nav-link" href="#4">4. Limitations contractuelles sur les données techniques</a>
                    <a class="nav-link" href="#5">5. Propriété intellectuelle et contrefaçons</a>
                    <a class="nav-link" href="#6">6. Limitations de responsabilité.</a>
                    <a class="nav-link" href="#7">7. Gestion des données personnelles</a>
                    <a class="nav-link" href="#8">8. Liens hypertextes et cookies</a>
                    <a class="nav-link" href="#9">9. Droit applicable et attribution de juridiction</a>
                    <a class="nav-link" href="#10">10. Les principales lois concernées</a>
                    <a class="nav-link" href="#11">11. Lexique</a>
                </nav>
            </div>
            <div class="col col-md">
                <div id="1">
                    <h4 class="font-weight-bold text-uppercase">1. Présentation du site</h4>
                    <p class="text-muted">
                        <strong>Propriétaire</strong> : Team Dismap – 2 rue bayard, 05000 GAP <br>
                        <strong>Créateur</strong>  : Team Dismap <br>
                        <strong>Responsable publication</strong> : Team Dismap – contact@dismap.fr <br>
                        Le responsable publication est une personne physique ou une personne morale. <br>
                        <strong>Webmaster</strong> : Team Dismap<br>
                        <strong>Hébergeur</strong> : <a href="https://www.ionos.fr/" target="_blank">1&1 Internet SARL</a> - 7, place de la Gare BP 70109 - 57200 Sarreguemines Cedex<br>
                        Crédits : Yann DEROUES / Lucas ALARCON / Laetitia MONSERRAT / Romain MORANGES <br>
                    </p>
                </div>
                <div id="2">
                    <h4 class="font-weight-bold text-uppercase">2. Conditions générales d’utilisation du site et des services proposés</h4>
                    <p class="text-muted">
                        L’utilisation du site <a href="https://www.dismap.fr/">https://www.dismap.fr/</a> implique l’acceptation pleine et entière des conditions générales d’utilisation ci-après décrites. Ces conditions d’utilisation sont susceptibles d’être modifiées ou complétées à tout moment, les utilisateurs du site <a href="https://www.dismap.fr/">https://www.dismap.fr/</a> sont donc invités à les consulter de manière régulière.
                    </p>
                    <p class="text-muted">
                        Ce site est normalement accessible à tout moment aux utilisateurs. Une interruption pour raison de maintenance technique peut être toutefois décidée par MIW, qui s’efforcera alors de communiquer préalablement aux utilisateurs les dates et heures de l’intervention.
                    </p>
                    <p class="text-muted">
                        Le site <a href="https://www.dismap.fr/">https://www.dismap.fr/</a> est mis à jour régulièrement par Team Dismap. De la même façon, les mentions légales peuvent être modifiées à tout moment : elles s’imposent néanmoins à l’utilisateur qui est invité à s’y référer le plus souvent possible afin d’en prendre connaissance.
                    </p>
                </div>
                <div id="3">
                    <h4 class="font-weight-bold text-uppercase">3. Description des services fournis</h4>
                    <p class="text-muted">
                        Le site <a href="https://www.dismap.fr/">https://www.dismap.fr/</a> a pour objet de fournir une information concernant l’ensemble des activités de la société.
                    </p>
                    <p class="text-muted">
                        MIW s’efforce de fournir sur le site <a href="https://www.dismap.fr/">https://www.dismap.fr/</a> des informations aussi précises que possible. Toutefois, il ne pourra être tenue responsable des omissions, des inexactitudes et des carences dans la mise à jour, qu’elles soient de son fait ou du fait des tiers partenaires qui lui fournissent ces informations.
                    </p>
                    <p class="text-muted">
                        Tous les informations indiquées sur le site <a href="https://www.dismap.fr/">https://www.dismap.fr/</a> sont données à titre indicatif, et sont susceptibles d’évoluer. Par ailleurs, les renseignements figurant sur le site <a href="https://www.dismap.fr/">https://www.dismap.fr/</a> ne sont pas exhaustifs. Ils sont donnés sous réserve de modifications ayant été apportées depuis leur mise en ligne.
                    </p>
                </div>
                <div id="4">
                    <h4 class="font-weight-bold text-uppercase">4. Limitations contractuelles sur les données techniques</h4>
                    <p class="text-muted">
                        Le site utilise la technologie JavaScript.
                    </p>
                    <p class="text-muted">
                        Le site Internet ne pourra être tenu responsable de dommages matériels liés à l’utilisation du site. De plus, l’utilisateur du site s’engage à accéder au site en utilisant un matériel récent, ne contenant pas de virus et avec un navigateur de dernière génération mis-à-jour
                    </p>
                </div>
                <div id="5">
                    <h4 class="font-weight-bold text-uppercase">5. Propriété intellectuelle et contrefaçons</h4>
                    <p class="text-muted">
                        MIW est propriétaire des droits de propriété intellectuelle ou détient les droits d’usage sur tous les éléments accessibles sur le site, notamment les textes, images, graphismes, logo, icônes, sons, logiciels.
                    </p>
                    <p class="text-muted">
                        Toute reproduction, représentation, modification, publication, adaptation de tout ou partie des éléments du site, quel que soit le moyen ou le procédé utilisé, est interdite, sauf autorisation écrite préalable de : MIW.
                    </p>
                    <p class="text-muted">
                        Toute exploitation non autorisée du site ou de l’un quelconque des éléments qu’il contient sera considérée comme constitutive d’une contrefaçon et poursuivie conformément aux dispositions des articles L.335-2 et suivants du Code de Propriété Intellectuelle.
                    </p>
                </div>
                <div id="6">
                    <h4 class="font-weight-bold text-uppercase">6. Limitations de responsabilité</h4>
                    <p class="text-muted">
                        MIW ne pourra être tenue responsable des dommages directs et indirects causés au matériel de l’utilisateur, lors de l’accès au site https://www.dismap.fr/, et résultant soit de l’utilisation d’un matériel ne répondant pas aux spécifications indiquées au point 4, soit de l’apparition d’un bug ou d’une incompatibilité.
                    </p>
                    <p class="text-muted">
                        MIW ne pourra également être tenue responsable des dommages indirects (tels par exemple qu’une perte de marché ou perte d’une chance) consécutifs à l’utilisation du site <a href="https://www.dismap.fr/">https://www.dismap.fr/</a>.
                    </p>
                    <p class="text-muted">
                        Des espaces interactifs (possibilité de poser des questions dans l’espace contact) sont à la disposition des utilisateurs. MIW se réserve le droit de supprimer, sans mise en demeure préalable, tout contenu déposé dans cet espace qui contreviendrait à la législation applicable en France, en particulier aux dispositions relatives à la protection des données. Le cas échéant, MIW se réserve également la possibilité de mettre en cause la responsabilité civile et/ou pénale de l’utilisateur, notamment en cas de message à caractère raciste, injurieux, diffamant, ou pornographique, quel que soit le support utilisé (texte, photographie…).
                    </p>
                </div>
                <div id="7">
                    <h4 class="font-weight-bold text-uppercase">7. Gestion des données personnelles</h4>
                    <p class="text-muted">
                        En France, les données personnelles sont notamment protégées par la loi n° 78-87 du 6 janvier 1978, la loi n° 2004-801 du 6 août 2004, l'article L. 226-13 du Code pénal et la Directive Européenne du 24 octobre 1995.
                    </p>
                    <p class="text-muted">
                        A l'occasion de l'utilisation du site <a href="https://www.dismap.fr/">https://www.dismap.fr/</a>, peuvent êtres recueillies : l'URL des liens par l'intermédiaire desquels l'utilisateur a accédé au site <a href="https://www.dismap.fr/">https://www.dismap.fr/</a>, le fournisseur d'accès de l'utilisateur, l'adresse de protocole Internet (IP) de l'utilisateur.
                    </p>
                    <p class="text-muted">
                        En tout état de cause MIW ne collecte des informations personnelles relatives à l'utilisateur que pour le besoin de certains services proposés par le site <a href="https://www.dismap.fr/">https://www.dismap.fr/</a>. L'utilisateur fournit ces informations en toute connaissance de cause, notamment lorsqu'il procède par lui-même à leur saisie. Il est alors précisé à l'utilisateur du site <a href="https://www.dismap.fr/">https://www.dismap.fr/</a> l’obligation ou non de fournir ces informations.
                    </p>
                    <p class="text-muted">
                        Conformément aux dispositions des articles 38 et suivants de la loi 78-17 du 6 janvier 1978 relative à l’informatique, aux fichiers et aux libertés, tout utilisateur dispose d’un droit d’accès, de rectification et d’opposition aux données personnelles le concernant, en effectuant sa demande écrite et signée, accompagnée d’une copie du titre d’identité avec signature du titulaire de la pièce, en précisant l’adresse à laquelle la réponse doit être envoyée.
                    </p>
                    <p class="text-muted">
                        Aucune information personnelle de l'utilisateur du site <a href="https://www.dismap.fr/">https://www.dismap.fr/</a> n'est publiée à l'insu de l'utilisateur, échangée, transférée, cédée ou vendue sur un support quelconque à des tiers. Seule l'hypothèse du rachat de MIW et de ses droits permettrait la transmission des dites informations à l'éventuel acquéreur qui serait à son tour tenu de la même obligation de conservation et de modification des données vis à vis de l'utilisateur du site <a href="https://www.dismap.fr/">https://www.dismap.fr/</a>.
                    </p>
                    <p class="text-muted">
                        Le site n'est pas déclaré à la CNIL car il ne recueille pas d'informations personnelles.
                    </p>
                    <p class="text-muted">
                        Les bases de données sont protégées par les dispositions de la loi du 1er juillet 1998 transposant la directive 96/9 du 11 mars 1996 relative à la protection juridique des bases de données.
                    </p>
                </div>
                <div id="8">
                    <h4 class="font-weight-bold text-uppercase">8. Liens hypertextes et cookies</h4>
                    <p class="text-muted">
                        Le site <a href="https://www.dismap.fr/">https://www.dismap.fr/</a> contient un certain nombre de liens hypertextes vers d’autres sites, mis en place avec l’autorisation de MIW. Cependant, MIW n’a pas la possibilité de vérifier le contenu des sites ainsi visités, et n’assumera en conséquence aucune responsabilité de ce fait.
                    </p>
                    <p class="text-muted">
                        La navigation sur le site <a href="https://www.dismap.fr/">https://www.dismap.fr/</a> est susceptible de provoquer l’installation de cookie(s) sur l’ordinateur de l’utilisateur. Un cookie est un fichier de petite taille, qui ne permet pas l’identification de l’utilisateur, mais qui enregistre des informations relatives à la navigation d’un ordinateur sur un site. Les données ainsi obtenues visent à faciliter la navigation ultérieure sur le site, et ont également vocation à permettre diverses mesures de fréquentation.
                    </p>
                    <p class="text-muted">
                        Le refus d’installation d’un cookie peut entraîner l’impossibilité d’accéder à certains services. L’utilisateur peut toutefois configurer son ordinateur de la manière suivante, pour refuser l’installation des cookies :
                    </p>
                    <p class="text-muted">
                        <strong>Sous Internet Explorer</strong> : onglet outil (pictogramme en forme de rouage en haut a droite) / options internet. Cliquez sur Confidentialité et choisissez Bloquer tous les cookies. Validez sur Ok.
                    </p>
                    <p class="text-muted">
                        <strong>Sous Firefox</strong> : en haut de la fenêtre du navigateur, cliquez sur le bouton Firefox, puis aller dans l'onglet Options. Cliquer sur l'onglet Vie privée.
                        Paramétrez les Règles de conservation sur :  utiliser les paramètres personnalisés pour l'historique. Enfin décochez-la pour  désactiver les cookies.
                    </p>
                    <p class="text-muted">
                        <strong>Sous Safari</strong> : Cliquez en haut à droite du navigateur sur le pictogramme de menu (symbolisé par un rouage). Sélectionnez Paramètres. Cliquez sur Afficher les paramètres avancés. Dans la section "Confidentialité", cliquez sur Paramètres de contenu. Dans la section "Cookies", vous pouvez bloquer les cookies.
                    </p>
                    <p class="text-muted">
                        <strong>Sous Chrome</strong> : Cliquez en haut à droite du navigateur sur le pictogramme de menu (symbolisé par trois lignes horizontales). Sélectionnez Paramètres. Cliquez sur Afficher les paramètres avancés. Dans la section "Confidentialité", cliquez sur préférences.  Dans l'onglet "Confidentialité", vous pouvez bloquer les cookies.
                    </p>
                </div>
                <div id="9">
                    <h4 class="font-weight-bold text-uppercase">9. Droit applicable et attribution de juridiction</h4>
                    <p class="text-muted">
                        Tout litige en relation avec l’utilisation du site <a href="https://www.dismap.fr/">https://www.dismap.fr/</a> est soumis au droit français. Il est fait attribution exclusive de juridiction aux tribunaux compétents de Paris.
                    </p>
                </div>
                <div id="10">
                    <h4 class="font-weight-bold text-uppercase">10. Les principales lois concernées</h4>
                    <p class="text-muted">
                        Loi n° 78-17 du 6 janvier 1978, notamment modifiée par la loi n° 2004-801 du 6 août 2004 relative à l'informatique, aux fichiers et aux libertés.
                    </p>
                    <p class="text-muted">
                        Loi n° 2004-575 du 21 juin 2004 pour la confiance dans l'économie numérique.
                    </p>
                </div>
                <div id="11">
                    <h4 class="font-weight-bold text-uppercase">11. Lexique</h4>
                    <p class="text-muted">
                        Utilisateur : Internaute se connectant, utilisant le site susnommé.
                    </p>
                    <p class="text-muted">
                        Informations personnelles : « les informations qui permettent, sous quelque forme que ce soit, directement ou non, l'identification des personnes physiques auxquelles elles s'appliquent » (article 4 de la loi n° 78-17 du 6 janvier 1978).
                    </p>
                </div>
            </div>
        </div>
    </div>

    @include('layouts.footer')
    @include('layouts._js')
    <script src="{{ asset('js/stickyfill.min.js') }}"></script>
    <script>
        const elements = $('.sticky');
        Stickyfill.add(elements);

        $('a[href*="#"]')
            .not('[href="#"]')
            .not('[href="#0"]')
            .click(function(event) {
                // On-page links
                if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
                    // Figure out element to scroll to
                    let target = $(this.hash);
                    target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                    // Does a scroll target exist?
                    if (target.length) {
                        // Only prevent default if animation is actually gonna happen
                        event.preventDefault();
                        $('html, body').animate({
                            scrollTop: target.offset().top
                        }, 1000, function() {
                            // Callback after animation
                            // Must change focus!
                            let $target = $(target);
                            $target.focus();
                            if ($target.is(":focus")) { // Checking if the target was focused
                                return false;
                            } else {
                                $target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
                                $target.focus(); // Set focus again
                            }
                        });
                    }
                }
            });
    </script>
@endsection