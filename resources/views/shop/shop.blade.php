@extends('layouts.app')

@section('content')

    @include('layouts.navbar')

    <div class="container">
        <div class="row mt-5">
            <div class="col-12 col-md-4 mb-4">

                <h3 class="mb-5">Détails du magasin "{{ $shop->nom_mag }}"</h3>

                <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                    <a class="nav-link rounded-pill active" id="v-pills-edit-shops-tab" data-toggle="pill" href="#v-pills-edit-shops" role="tab" aria-controls="v-pills-edit-shops" aria-selected="true">
                        Modifier les informations du magasin
                    </a>
                    <a class="nav-link rounded-pill" id="v-pills-promo-tab" data-toggle="pill" href="#v-pills-promo" role="tab" aria-controls="v-pills-promo" aria-selected="false">
                        Promotions du magasin
                    </a>
                    <a href="{{ route('resp_shops') }}" class="nav-link">
                        Retour
                    </a>
                </div>
            </div>

            <div class="col-12 col-md">
                <div class="tab-content" id="v-pills-tabContent">
                    <div class="tab-pane fade show active" id="v-pills-edit-shops" role="tabpanel" aria-labelledby="v-pills-edit-shops-tab">

                        @if(session()->has('messageMag'))
                            <div class="alert alert-success rounded-pill alert-dismissible fade show" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <strong>
                                    <i class="fas fa-check mr-2"></i>{{ session()->get('messageMag') }}
                                </strong>
                            </div>
                        @endif

                        @if(session()->has('messageMagErr'))
                            <div class="alert alert-danger rounded-pill alert-dismissible fade show" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <strong>
                                    <i class="fas fa-exclamation mr-2"></i>{{ session()->get('messageMagErr') }}
                                </strong>
                            </div>
                        @endif

                        <form method="post" enctype="multipart/form-data" action="{{ route('edit_shop_post', ['id_shop' => $shop->id]) }}" class="needs-validation" novalidate>

                            {{csrf_field()}}

                            <div class="card mb-4">
                                <div class="card-body">
                                    <div class="card-title mb-4">
                                        <i class="fas fa-industry mr-2"></i>Informations
                                    </div>

                                    <div class="form-group">
                                        <label for="nom_mag" class="font-weight-bold">Nom*</label>
                                        <input type="text" class="form-control rounded-pill" id="nom_mag" name="nom_mag" value="{{ old('nom_mag', $shop->nom_mag) }}" required>
                                        @if ($errors->has('nom_mag'))
                                            <div class="invalid-feedback d-block">
                                                {{ $errors->first('nom_mag') }}
                                            </div>
                                        @endif
                                    </div>

                                    <div class="form-row">
                                        <div class="form-group col-12 col-md">
                                            <label for="mail_mag" class="font-weight-bold">Email*</label>
                                            <input type="email" class="form-control rounded-pill" id="mail_mag" name="mail_mag" value="{{ old('mail_mag', $shop->mail_mag) }}" required>
                                            @if ($errors->has('mail_mag'))
                                                <div class="invalid-feedback d-block">
                                                    {{ $errors->first('mail_mag') }}
                                                </div>
                                            @endif
                                        </div>

                                        <div class="form-group col col-md">
                                            <label for="tel_mag" class="font-weight-bold">Téléphone*</label>
                                            <input type="tel" class="form-control rounded-pill" id="tel_mag" name="tel_mag" pattern="^((\+\d{1,3}(-| )?\(?\d\)?(-| )?\d{1,5})|(\(?\d{2,6}\)?))(-| )?(\d{3,4})(-| )?(\d{4})(( x| ext)\d{1,5}){0,1}$" value="{{ old('tel_mag', $shop->tel_mag) }}" required>
                                            @if ($errors->has('tel_mag'))
                                                <div class="invalid-feedback d-block">
                                                    {{ $errors->first('tel_mag') }}
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-12 col-md">
                                            <label for="siret_mag" class="font-weight-bold">SIRET*</label>
                                            <input type="text" class="form-control rounded-pill" id="siret_mag" name="siret_mag" value="{{ old('siret_mag', $shop->siret_mag) }}" required>
                                            @if ($errors->has('siret_mag'))
                                                <div class="invalid-feedback d-block">
                                                    {{ $errors->first('siret_mag') }}
                                                </div>
                                            @endif
                                        </div>

                                        <div class="form-group col col-md">
                                            <label for="photo1_mag" class="font-weight-bold">Photo du magasin</label>
                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input" id="photo1_mag" name="photo1_mag">
                                                <label for="photo1_mag" class="custom-file-label rounded-pill">Photo du magasin</label>
                                            </div>
                                            @if ($errors->has('photo1_mag'))
                                                <div class="invalid-feedback d-block">
                                                    {{ $errors->first('photo1_mag') }}
                                                </div>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <a data-toggle="collapse" href="#photo_mag" role="button" aria-expanded="false" aria-controls="collapse_photo_mag" class="font-weight-bold">Voir l'image actuelle</a>
                                        <div class="collapse mt-3" id="photo_mag">
                                            <img src="/uploads/magasins/{{ $shop->photo1_mag }}" alt="Image du magasin" class="img-fluid rounded shadow">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card mb-4">
                                <div class="card-body">
                                    <div class="card-title mb-4">
                                        <i class="fas fa-map-pin mr-2"></i>Localisation
                                    </div>

                                    <div class="form-group">
                                        <label for="villes" class="font-weight-bold">Ville*</label>
                                        <input id="villes" list="villeDatalist" type="text" name="id_ville" value="{{ $shop_ville[0]->nom_ville .' & '. $shop_ville[0]->cp_ville }}" class="form-control rounded-pill font-weight-bold" placeholder="Entrer une ville">
                                        <datalist id="villeDatalist"></datalist>
                                        @if ($errors->has('id_ville'))
                                            <div class="invalid-feedback d-block">
                                                {{ $errors->first('id_ville') }}
                                            </div>
                                        @endif
                                    </div>

                                    <div class="form-group">
                                        <label for="ad1_mag" class="font-weight-bold">Adresse 1*</label>
                                        <input type="text" class="form-control rounded-pill" id="ad1_mag" name="ad1_mag" value="{{ old('ad1_mag', $shop->ad1_mag) }}" required>
                                        @if ($errors->has('ad1_mag'))
                                            <div class="invalid-feedback d-block">
                                                {{ $errors->first('ad1_mag') }}
                                            </div>
                                        @endif
                                    </div>

                                    <div class="form-group">
                                        <label for="ad2_mag" class="font-weight-bold">Adresse 2</label>
                                        <input type="text" class="form-control rounded-pill" id="ad2_mag" name="ad2_mag" value="{{ old('ad2_mag', $shop->ad2_mag) }}">
                                        @if ($errors->has('ad2_mag'))
                                            <div class="invalid-feedback d-block">
                                                {{ $errors->first('ad2_mag') }}
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="card mb-5">
                                <div class="card-body">
                                    <div class="card-title mb-4">
                                        <i class="fas fa-compress-arrows-alt mr-2"></i>Secteur d'activité
                                    </div>

                                    <div class="form-group">
                                        <label for="id_type" class="font-weight-bold">Type*</label>
                                        <select name="id_type" id="id_type" class="custom-select rounded-pill" required>
                                            @foreach ($types as $type)
                                                <option value="{{ $type->id }}">{{ $type->libelle_type }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label for="id_cat" class="font-weight-bold">Catégorie</label>
                                        <select name="id_cat" id="id_cat" class="custom-select rounded-pill" required>
                                            @foreach ($categories as $categorie)
                                                <option value="{{ $categorie->id }}">{{ $categorie->libelle_cat }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <button type="submit" class="btn btn-primary btn-pill">Valider</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="tab-pane fade" id="v-pills-promo" role="tabpanel" aria-labelledby="v-pills-promo-tab">

                        @if(session()->has('messagePromo'))
                            <div class="alert alert-success rounded-pill alert-dismissible fade show" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <strong>
                                    <i class="fas fa-check mr-2"></i>{{ session()->get('messagePromo') }}
                                </strong>
                            </div>
                        @endif

                        @if (count($promotions) === 0)
                            <div class="alert alert-warning shadow rounded">
                                <div class="font-weight-bold text-black">
                                    <i class="fas fa-exclamation-triangle mr-2"></i>Aucune promotion n'a été trouvée !
                                </div>
                            </div>
                        @endif

                        <button type="button" data-toggle="modal" data-target="#modalPromo" class="btn btn-primary btn-lg btn-block btn-pill shadow-sm font-weight-bold mb-4">
                            <i class="fas fa-plus mr-2"></i>Ajouter une promotion
                        </button>

                        <div class="card mb-5">
                            <div class="card-body">
                                <table id="table" class="table table-hover table-responsive-sm">
                                    <thead>
                                        <tr>
                                            <th scope="col">Libellé</th>
                                            <th scope="col">Date d'activation</th>
                                            <th scope="col">Date d'expiration</th>
                                            <th scope="col">Code Avis</th>
                                            <th scope="col">Etat</th>
                                            <th scope="col">Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($promotions as $promotion)
                                            <tr>
                                                <td>
                                                    {{ substr($promotion->libelle_promo,0,10) }}
                                                </td>
                                                <td>{{ $promotion->date_debut_promo }}</td>
                                                <td>{{ $promotion->date_fin_promo }}</td>
                                                <td>{{ $promotion->code_avis_promo }}</td>
                                                <td data-order="{{ $promotion->etat_promo ? 1 : 0 }}">
                                                    <i class="fas fa-circle {{ $promotion->etat_promo ? 'text-success' : 'text-danger' }}"></i>
                                                </td>
                                                <td>
                                                    <a href="{{ route('edit_promo', ['id_promo' => $promotion->id]) }}"><i class="fas fa-edit"></i></a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="modalPromo" tabindex="-1" role="dialog" aria-labelledby="modalPromoTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalPromoTitle">Ajouter une promotion</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form enctype="multipart/form-data" method="post" action="{{ route('add_promo_post') }}" class="needs-validation" novalidate>
                    <div class="modal-body">

                        {{ csrf_field() }}

                        <input type="hidden" name="id_mag" value="{{ $shop->id }}">

                        <div class="form-group">
                            <label for="libelle_promo" class="font-weight-bold">Nom promotion*</label>
                            <input type="text" class="form-control rounded-pill" name="libelle_promo" id="libelle_promo" required>
                            @if ($errors->has('libelle_promo'))
                                <div class="invalid-feedback d-block">
                                    {{ $errors->first('libelle_promo') }}
                                </div>
                            @endif
                        </div>

                        <div class="form-row">
                            <div class="form-group col-12 col-md">
                                <label for="date_promo" class="font-weight-bold">Période d'activation* <span data-toggle="tooltip" data-placement="right" title="Attention, vous ne pourrez plus modifier cette date !"><i class="fas fa-exclamation-triangle text-danger"></i></span></label>
                                <div class="input-daterange input-group" id="date_promo">
                                    <input type="text" class="form-control" name="date_debut_promo" id="date_debut_promo" placeholder="Date d'activation" value="{{ old('date_debut_promo') }}" required>
                                    @if ($errors->has('date_debut_promo'))
                                        <div class="invalid-feedback d-block">
                                            {{ $errors->first('date_debut_promo') }}
                                        </div>
                                    @endif
                                    <input type="text" class="form-control" name="date_fin_promo" id="date_fin_promo" placeholder="Date d'expiration" value="{{ old('date_fin_promo') }}" required>
                                    @if ($errors->has('date_fin_promo'))
                                        <div class="invalid-feedback d-block">
                                            {{ $errors->first('date_fin_promo') }}
                                        </div>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group col-12 col-md-3">
                                <label for="etat_promo" class="text-capitalize font-weight-bold">état*</label>
                                <div class="custom-control custom-toggle">
                                    <input type="checkbox" id="etat_promo" name="etat_promo" class="custom-control-input" checked>
                                    <label class="custom-control-label text-capitalize font-weight-bold" id="statusText" for="etat_promo">Activée</label>
                                </div>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-12 col-md">
                                <label for="photo1_promo" class="font-weight-bold">Photo 1*</label>
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="photo1_promo" name="photo1_promo" required>
                                    <label class="custom-file-label rounded-pill" for="photo1_promo">Photo 1 (Obligatoire)</label>
                                </div>
                                @if ($errors->has('photo1_promo'))
                                    <div class="invalid-feedback d-block">
                                        {{ $errors->first('photo1_promo') }}
                                    </div>
                                @endif
                            </div>
                            <div class="form-group col-12 col-md">
                                <label for="photo1_promo" class="font-weight-bold">Photo 2</label>
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="photo2_promo" name="photo2_promo">
                                    <label class="custom-file-label rounded-pill" for="photo2_promo">Photo 2 (Facultatif)</label>
                                </div>
                                @if ($errors->has('photo2_promo'))
                                    <div class="invalid-feedback d-block">
                                        {{ $errors->first('photo2_promo') }}
                                    </div>
                                @endif
                            </div>
                            <div class="form-group col col-md">
                                <label for="photo3_promo" class="font-weight-bold">Photo 3</label>
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="photo3_promo" name="photo3_promo">
                                    <label class="custom-file-label rounded-pill" for="photo3_promo">Photo 3 (Facultatif)</label>
                                </div>
                                @if ($errors->has('photo3_promo'))
                                    <div class="invalid-feedback d-block">
                                        {{ $errors->first('photo3_promo') }}
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary btn-pill" data-dismiss="modal">
                            Fermer
                        </button>
                        <button type="submit" class="btn btn-primary btn-pill">
                            Valider
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    @include('layouts._js')
    <script src="{{ asset('js/datatables.min.js') }}"></script>
    <script src="{{ asset('js/axios.min.js') }}"></script>
    <script>
        let input = document.getElementById('villes');
        let datalist = document.getElementById('villeDatalist');
        let memoryReq;

        let errorCity = () =>{return console.log('pb ville')};

        let findCityDone = (r) =>{
            let rep = r.data;

            datalist.innerHTML="";

            /*On actualise memoryReq que si rep retourne un résultat*/
            if (rep['cities'].length !== 0) {memoryReq = rep['cities'];}

            if(rep['cities'].length !== 0) {
                for (let i=0; i<rep['cities'].length; i++){
                    let option = datalist.appendChild(document.createElement('option'));
                    option.setAttribute('value', rep['cities'][i].nom_ville+' & '+rep['cities'][i].cp_ville);
                }
            }
        };

        function findCity(inputValue){
            axios.post(
                '{{route('city')}}',
                {inputValue:inputValue,
                    _token: '{{ csrf_token() }}'
                }
            )
                .then(findCityDone)
                .catch(errorCity);
        }

        function startFindCity(){
            if(input.value.length > 2){
                findCity(input.value);
            } else if (datalist.childElementCount) {
                datalist.innerHTML="";
            }
        }

        document.addEventListener('DOMContentLoaded', function(){

            /*On ajoute un évènement startFindCity sur l'input quand on tape sur le clavier*/
            input.addEventListener('keyup', function(){startFindCity();});
            // /*On ajoute le même évènement sur la datalist au clic*/
            datalist.addEventListener('click', function(){startFindCity();});
            datalist.addEventListener('keyup', function(){startFindCity();});
        });

        @if(session()->has('messagePromo'))
            $('#v-pills-tab a[href="#v-pills-promo"]').tab('show');
        @endif

        @if($errors->has('nom_mag') ||
            $errors->has('mail_mag') ||
            $errors->has('tel_mag') ||
            $errors->has('siret_mag') ||
            $errors->has('photo1_mag') ||
            $errors->has('ad1_mag') ||
            $errors->has('ad2_mag'))
            $('#v-pills-tab a[href="#v-pills-edit-shop"]').tab('show');
        @elseif($errors->has('libelle_promo') ||
            $errors->has('date_debut_promo') ||
            $errors->has('date_fin_promo') ||
            $errors->has('photo1_promo') ||
            $errors->has('photo2_promo') ||
            $errors->has('photo3_promo'))
            $('#v-pills-tab a[href="#v-pills-promo"]').tab('show');
            $('#modalPromo').modal('show');
        @endif

        $(document).ready(function () {
            !function (a) {
                a.fn.datepicker.dates.fr = {
                    days: ["dimanche", "lundi", "mardi", "mercredi", "jeudi", "vendredi", "samedi"],
                    daysShort: ["dim.", "lun.", "mar.", "mer.", "jeu.", "ven.", "sam."],
                    daysMin: ["d", "l", "ma", "me", "j", "v", "s"],
                    months: ["janvier", "février", "mars", "avril", "mai", "juin", "juillet", "août", "septembre", "octobre", "novembre", "décembre"],
                    monthsShort: ["janv.", "févr.", "mars", "avril", "mai", "juin", "juil.", "août", "sept.", "oct.", "nov.", "déc."],
                    today: "Aujourd'hui",
                    monthsTitle: "Mois",
                    clear: "Effacer",
                    weekStart: 1,
                    format: "dd/mm/yyyy"
                }
            }(jQuery);

            $(function () {
                $('[data-toggle="tooltip"]').tooltip()
            });

            $('#date_promo').datepicker({
                language: 'fr',
                todayBtn: 'linked',
                format: 'yyyy-mm-dd',
                startDate: Date()
            });

            $('#date_debut_promo').datepicker({})
            .on('changeDate', function (e) {
                const startDate = new Date(e.date.valueOf());
                let endDate = new Date(startDate);
                endDate = new Date(endDate.setDate(endDate.getDate() + 14));
                $('#date_fin_promo').datepicker('setEndDate', endDate);
            });

            $('#etat_promo').click(function () {
                $('#etat_promo').prop('checked') ? $('#statusText').text('Activée') : $('#statusText').text('Désactivée');
            });

            $('#table').DataTable({
                language: {
                    processing: "Traitement en cours...",
                    search: "Rechercher&nbsp;:",
                    lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
                    info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                    infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
                    infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                    infoPostFix: "",
                    loadingRecords: "Chargement en cours...",
                    zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
                    emptyTable: "Aucune donnée disponible dans le tableau",
                    paginate: {
                        first: "Premier",
                        previous: "Pr&eacute;c&eacute;dent",
                        next: "Suivant",
                        last: "Dernier"
                    },
                    aria: {
                        sortAscending: ": activer pour trier la colonne par ordre croissant",
                        sortDescending: ": activer pour trier la colonne par ordre décroissant"
                    }
                }
            });
        });
    </script>
@endsection
