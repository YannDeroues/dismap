@extends('layouts.app')

@section('content')

    @include('layouts.navbar')

    <div class="container">
        <div class="row mt-5">
            <div class="col-12 col-md-4 mb-4">

                <h3 class="mb-5">Gestion magasins</h3>

                <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                    <a class="nav-link rounded-pill active" id="v-pills-shops-tab" data-toggle="pill" href="#v-pills-shops" role="tab" aria-controls="v-pills-shops" aria-selected="true">
                        Mes magasins
                    </a>
                    <a class="nav-link rounded-pill" id="v-pills-add-shop-tab" data-toggle="pill" href="#v-pills-add-shop" role="tab" aria-controls="v-pills-add-shop" aria-selected="false">
                        Ajouter un magasin
                    </a>
                </div>
            </div>
            <div class="col-12 col-md">
                @if(session()->has('messageMag'))
                    <div class="alert alert-success rounded-pill alert-dismissible fade show" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <strong>
                            <i class="fas fa-check mr-2"></i>{{ session()->get('messageMag') }}
                        </strong>
                    </div>
                @endif
                @if(session()->has('messageMagErr'))
                    <div class="alert alert-danger rounded-pill alert-dismissible fade show" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <strong>
                            <i class="fas fa-exclamation mr-2"></i>{{ session()->get('messageMagErr') }}
                        </strong>
                    </div>
                @endif
                <div class="tab-content" id="v-pills-tabContent">
                    <div class="tab-pane fade show active" id="v-pills-shops" role="tabpanel" aria-labelledby="v-pills-shops-tab">
                        <div class="list-group shadow rounded">
                            @if (count($shops) === 0)
                                <div class="list-group-item list-group-item-action font-weight-bold active"><i class="fas fa-exclamation-triangle mr-2"></i>Aucun magasin n'a été trouvé !</div>
                            @endif
                            @foreach ($shops as $shop)
                                <a href="{{ route('shop', ['id_shop' => $shop->id]) }}" class="list-group-item list-group-item-action border-0 font-weight-bolder text-capitalize"><i class="fas fa-chevron-right mr-2"></i>{{ $shop->nom_mag }}</a>
                            @endforeach
                        </div>
                    </div>
                    <div class="tab-pane fade" id="v-pills-add-shop" role="tabpanel" aria-labelledby="v-pills-add-shop-tab">
                        <form method="post" enctype="multipart/form-data" action="{{ route('add_shop_post') }}" class="needs-validation" novalidate>

                            {{ csrf_field() }}

                            <div class="card mb-4">
                                <div class="card-body">
                                    <div class="card-title mb-4">
                                        <i class="fas fa-industry mr-2"></i>Informations
                                    </div>

                                    <div class="form-group">
                                        <label for="nom_mag" class="font-weight-bold">Nom*</label>
                                        <input type="text" class="form-control rounded-pill" id="nom_mag" name="nom_mag" value="{{ old('nom_mag') }}" required>
                                        @if ($errors->has('nom_mag'))
                                            <div class="invalid-feedback d-block">
                                                {{ $errors->first('nom_mag') }}
                                            </div>
                                        @endif
                                    </div>

                                    <div class="form-row">
                                        <div class="form-group col-12 col-md">
                                            <label for="mail_mag" class="font-weight-bold">Email*</label>
                                            <input type="email" class="form-control rounded-pill" id="mail_mag" name="mail_mag" value="{{ old('mail_mag') }}" required>
                                            @if ($errors->has('mail_mag'))
                                                <div class="invalid-feedback d-block">
                                                    {{ $errors->first('mail_mag') }}
                                                </div>
                                            @endif
                                        </div>

                                        <div class="form-group col col-md">
                                            <label for="tel_mag" class="font-weight-bold">Téléphone*</label>
                                            <input type="tel" class="form-control rounded-pill" id="tel_mag" name="tel_mag" value="{{ old('tel_mag') }}" pattern="^((\+\d{1,3}(-| )?\(?\d\)?(-| )?\d{1,5})|(\(?\d{2,6}\)?))(-| )?(\d{3,4})(-| )?(\d{4})(( x| ext)\d{1,5}){0,1}$" required>
                                            @if ($errors->has('tel_mag'))
                                                <div class="invalid-feedback d-block">
                                                    {{ $errors->first('tel_mag') }}
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-12 col-md">
                                            <label for="siret_mag" class="font-weight-bold">SIRET*</label>
                                            <input type="text" class="form-control rounded-pill" id="siret_mag" name="siret_mag" value="{{ old('siret_mag') }}" required>
                                            @if ($errors->has('siret_mag'))
                                                <div class="invalid-feedback d-block">
                                                    {{ $errors->first('siret_mag') }}
                                                </div>
                                            @endif
                                        </div>

                                        <div class="form-group col col-md">
                                            <label for="photo1_mag" class="font-weight-bold">Photo du magasin*</label>
                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input" id="photo1_mag" name="photo1_mag" required>
                                                <label for="photo1_mag" class="custom-file-label rounded-pill">Photo du magasin</label>
                                            </div>
                                            @if ($errors->has('photo1_mag'))
                                                <div class="invalid-feedback d-block">
                                                    {{ $errors->first('photo1_mag') }}
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="card mb-4">
                                <div class="card-body">
                                    <div class="card-title mb-4">
                                        <i class="fas fa-map-pin mr-2"></i>Localisation
                                    </div>

                                    <div class="form-group">
                                        <label for="villes" class="font-weight-bold">Ville*</label>
                                        <input id="villes" list="villeDatalist" type="text" name="id_ville" class="form-control rounded-pill font-weight-bold" placeholder="Entrer une ville">
                                        <datalist id="villeDatalist"></datalist>
                                        @if ($errors->has('id_ville'))
                                            <div class="invalid-feedback d-block">
                                                {{ $errors->first('id_ville') }}
                                            </div>
                                        @endif
                                    </div>

                                    <div class="form-group">
                                        <label for="ad1_mag" class="font-weight-bold">Adresse 1*</label>
                                        <input type="text" class="form-control rounded-pill" id="ad1_mag" name="ad1_mag" value="{{ old('ad1_mag') }}" required>
                                        @if ($errors->has('ad1_mag'))
                                            <div class="invalid-feedback d-block">
                                                {{ $errors->first('ad1_mag') }}
                                            </div>
                                        @endif
                                    </div>

                                    <div class="form-group">
                                        <label for="ad2_mag" class="font-weight-bold">Adresse 2</label>
                                        <input type="text" class="form-control rounded-pill" id="ad2_mag" name="ad2_mag" value="{{ old('ad2_mag') }}">
                                        @if ($errors->has('ad2_mag'))
                                            <div class="invalid-feedback d-block">
                                                {{ $errors->first('ad2_mag') }}
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="card mb-5">
                                <div class="card-body">
                                    <div class="card-title mb-4">
                                        <i class="fas fa-compress-arrows-alt mr-2"></i>Secteur d'activité
                                    </div>

                                    <div class="form-group">
                                        <label for="id_type" class="font-weight-bold">Type*</label>
                                        <select name="id_type" id="id_type" class="custom-select rounded-pill" required>
                                            @foreach ($types as $type)
                                                <option value="{{ $type->id }}">{{ $type->libelle_type }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label for="id_cat" class="font-weight-bold">Catégorie</label>
                                        <select name="id_cat" id="id_cat" class="custom-select rounded-pill" required>
                                            @foreach ($categories as $categorie)
                                                <option value="{{ $categorie->id }}">{{ $categorie->libelle_cat }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <button type="submit" class="btn btn-primary btn-pill">
                                        Valider
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('layouts._js')
    <script src="{{ asset('js/axios.min.js') }}"></script>
    <script>
        @if(!empty($errors->all()))
            $('#v-pills-tab a[href="#v-pills-add-shop"]').tab('show');
        @endif

        let input = document.getElementById('villes');
        let datalist = document.getElementById('villeDatalist');
        let memoryReq;

        let errorCity = () =>{return console.log('pb ville')};

        let findCityDone = (r) =>{
            let rep = r.data;

            datalist.innerHTML="";

            /*On actualise memoryReq que si rep retourne un résultat*/
            if (rep['cities'].length !== 0) {memoryReq = rep['cities'];}

            if(rep['cities'].length !== 0) {
                for (let i=0; i<rep['cities'].length; i++){
                    let option = datalist.appendChild(document.createElement('option'));
                    option.setAttribute('value', rep['cities'][i].nom_ville+' & '+rep['cities'][i].cp_ville);
                }
            }
        };

        function findCity(inputValue){
            axios.post(
                '{{route('city')}}',
                {inputValue:inputValue,
                    _token: '{{ csrf_token() }}'
                }
            )
            .then(findCityDone)
            .catch(errorCity);
        }

        function startFindCity(){
            if(input.value.length > 2){
                findCity(input.value);
            } else if (datalist.childElementCount) {
                datalist.innerHTML="";
            }
        }

        document.addEventListener('DOMContentLoaded', function(){

            /*On ajoute un évènement startFindCity sur l'input quand on tape sur le clavier*/
            input.addEventListener('keyup', function(){startFindCity();});
            // /*On ajoute le même évènement sur la datalist au clic*/
            datalist.addEventListener('click', function(){startFindCity();});
            datalist.addEventListener('keyup', function(){startFindCity();});
        });
    </script>

@endsection
