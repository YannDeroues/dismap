@extends('layouts.app')

@section('content')

    @include('layouts.navbar')

    <div class="py-4">
        <div class="contact--container-top py-5">
            <h2 class="text-uppercase">Contactez-nous</h2>
        </div>

        <div class="container mb-5">
            <div class="row">
                <div class="col-lg">
                    <p class="lead">
                        Vous souhaitez obtenir des informations sur le fonctionnement du site ? Une demande de partenariat ?
                    </p>
                    <p class="lead mb-5">
                        Veuillez remplir le formulaire ci-contre et nous vous répondrons dans les plus brefs délais.
                    </p>
                    <div class="row">
                        <div class="col-lg-12">
                            <h3 class="text-uppercase mb-4 text-secondary">Téléphone</h3>
                        </div>
                        <div class="col-1 text-center">
                            <span class="h3 text-secondary">
                                <i class="fas fa-phone"></i>
                            </span>
                        </div>
                        <div class="col">
                            <p class="h3 text-secondary">
                                04 52 10 20 30
                            </p>
                        </div>
                    </div>
                    <div class="row my-5">
                        <div class="col-lg-12">
                            <h3 class="text-uppercase mb-4 text-primary">Nos réseaux sociaux</h3>
                        </div>
                        <div class="col">
                            <div class="border-top my-4 border-primary"></div>
                        </div>
                        <div class="col text-center">
                            <a href="#" class="h3 text-decoration-none text-primary">
                                <i class="fab fa-facebook-square"></i>
                            </a>
                        </div>
                        <div class="col text-center">
                            <a href="#" class="h3 text-decoration-none text-primary">
                                <i class="fab fa-twitter"></i>
                            </a>
                        </div>
                        <div class="col text-center">
                            <a href="#" class="h3 text-decoration-none text-primary">
                                <i class="fab fa-instagram"></i>
                            </a>
                        </div>
                        <div class="col text-center">
                            <a href="#" class="h3 text-decoration-none text-primary">
                                <i class="fab fa-discord"></i>
                            </a>
                        </div>
                        <div class="col text-center">
                            <a href="#" class="h3 text-decoration-none text-primary">
                                <i class="fab fa-snapchat-ghost"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-lg">
                    <div class="card">
                        <div class="card-body">
                            <form method="post" action="#">
                                <div class="form-group">
                                    <label for="name" class="font-weight-bold">Nom*</label>
                                    <input type="text" class="form-control rounded-pill" id="name" name="name" required>
                                </div>
                                <div class="form-group">
                                    <label for="email" class="font-weight-bold">Email*</label>
                                    <input type="email" class="form-control rounded-pill" id="email" name="email" required>
                                </div>
                                <div class="form-group">
                                    <label for="message" class="font-weight-bold">Message*</label>
                                    <textarea class="form-control" name="message" id="message" cols="30" rows="5"></textarea>
                                </div>
                                <button type="submit" class="btn btn-primary font-weight-bold btn-pill btn-block btn-lg">Envoyer</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('layouts._js')

@endsection