<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>404 - Page introuvable</title>

        <link rel="icon" type="image/png" href="{{ asset('img/logo_s.png') }}">

        <!-- Styles -->
        <link href="{{ asset('bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
        <link href="{{ asset('css/main.css') }}" rel="stylesheet">
    </head>

    <body class="bg-error404">
        <div class="container-fluid">
            <div class="row vh-100 justify-content-center align-items-start">
                <div class="mt-5 text-center">
                    <h1 class="display-2 text-white font-weight-bold">4 0 4</h1>
                    <p class="lead text-white">La page que vous recherchez est introuvable.</p>
                    <a href="{{ url('/') }}" class="btn btn-light btn-pill btn-lg">Revenir à l'accueil</a>
                </div>
            </div>
        </div>
    </body>
</html>


