@extends('layouts.app')

@section('content')

    <body>

        @include('layouts.navbar')

        <div class="container mt-5">
            <div class="row">
                <div class="col-lg">
                    <div class="card mb-3">
                        <div class="row no-gutters">
                            <div class="col-md-12 col-lg-8">
                                <div id="carouselExampleIndicators" class="carousel slide shadow-none" data-ride="carousel">
                                    <ol class="carousel-indicators">
                                        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                                        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                                    </ol>
                                    <div class="carousel-inner rounded">
                                        <div class="carousel-item active">
                                            <img class="d-block w-100" src="http://via.placeholder.com/740x600" alt="Jules gap - Illustration 1">
                                        </div>
                                        <div class="carousel-item">
                                            <img class="d-block w-100" src="http://via.placeholder.com/740x600" alt="Jules gap - Illustration 2">
                                        </div>
                                    </div>
                                    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Next</span>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md col-lg">
                                <div class="card-body">
                                    <h4 class="card-title mb-4">Jules GAP</h4>
                                    <p class="lead"><i class="fas fa-bullhorn"></i> -10% sur la nouvelle collection</p>
                                    <div class="row">
                                        <div class="col-12 mb-3">
                                            <button class="btn btn-outline-secondary btn-block btn-pill btn-lg mb-3">Obtenir le code</button>
                                            <div class="progress-wrapper">
                                                <span class="progress-label">Expire le: {{ date('d/m/Y') }}</span>
                                                <div class="progress progress-lg my-3">
                                                    <div class="progress-bar bg-danger" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 90%;">
                                                        <span class="progress-value">2j restant</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col">
                                            <div class="card-icon-text">
                                                <span class="mx-3">
                                                      <i class="fas fa-map-pin"></i>
                                                </span>
                                                <p>2 Rue Elisée, 05000 Gap</p>
                                            </div>
                                            <div class="card-icon-text">
                                                <span class="mx-3">
                                                    <i class="fas fa-phone"></i>
                                                </span>
                                                <p>06 52 10 20 20</p>
                                            </div>
                                            <div class="card-icon-text">
                                                <span class="mx-3">
                                                    <i class="fas fa-at"></i>
                                                </span>
                                                <p>contact@jules.com</p>
                                            </div>
                                            <div class="card-icon-text">
                                                <span class="mx-3">
                                                    <i class="fas fa-star"></i>
                                                </span>
                                                <p>4.5 | <a href="{{ url('historique-avis') }}">25 avis</a></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @include('layouts.footer')
        @include('layouts._js')

@endsection