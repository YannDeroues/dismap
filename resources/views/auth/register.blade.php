<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'Dismap') }}</title>

        <link rel="icon" type="image/png" href="{{ asset('img/logo_s.png') }}">

        <!-- Styles -->
        <link href="{{ asset('bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
        <link rel="stylesheet" href="{{ asset('css/fontawesome.min.css') }}">
        <link href="{{ asset('css/main.css') }}" rel="stylesheet">
    </head>

    <body>
        <div class="container-fluid bg-color-auth">
            <div class="row">
                <div class="col-md-8 col-lg-6">
                    <div class="min-vh-100 d-flex align-items-center py-5">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-md-9 col-lg-8 mx-auto mt-5">
                                    <form method="POST" action="{{ route('register') }}" class="needs-validation" novalidate>

                                        {{ csrf_field() }}

                                        <div class="form-row">
                                            <div class="form-group col-lg">
                                                <label for="lastname" class="font-weight-bold">Nom*</label>
                                                <input id="lastname" type="text" class="form-control rounded-pill {{ $errors->has('lastname') ? ' is-invalid' : '' }}" name="lastname" required autofocus>

                                                @if ($errors->has('lastname'))
                                                    <div class="invalid-feedback d-block">
                                                        {{ $errors->first('lastname') }}
                                                    </div>
                                                @endif
                                            </div>
                                            <div class="form-group col-lg">
                                                <label for="firstname" class="font-weight-bold">Prénom*</label>
                                                <input id="firstname" type="text" class="form-control rounded-pill {{ $errors->has('firstname') ? ' is-invalid' : '' }}" name="firstname" required>

                                                @if ($errors->has('firstname'))
                                                    <div class="invalid-feedback d-block">
                                                        {{ $errors->first('firstname') }}
                                                    </div>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                            <label for="email" class="font-weight-bold">Adresse email*</label>
                                            <input id="email" type="email" class="form-control rounded-pill {{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                            @if ($errors->has('email'))
                                                <div class="invalid-feedback d-block">
                                                    {{ $errors->first('email') }}
                                                </div>
                                            @endif
                                        </div>

                                        <div class="form-row">
                                            <div class="form-group col-lg">
                                                <label for="password" class="font-weight-bold">Mot de passe*</label>
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span id="showPwd" class="btn btn-pill btn-primary">
                                                            <i class="far fa-eye" id="iconPwd"></i>
                                                        </span>
                                                    </div>
                                                    <input id="password" type="password" class="form-control form-password rounded-pill {{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                                    @if ($errors->has('password'))
                                                        <div class="invalid-feedback d-block">
                                                            {{ $errors->first('password') }}
                                                        </div>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="form-group col-lg">
                                                <label for="password_confirm" class="font-weight-bold">Confirmation*</label>
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span id="showPwdConf" class="btn btn-pill btn-primary">
                                                            <i class="far fa-eye" id="iconPwdConf"></i>
                                                        </span>
                                                    </div>
                                                    <input id="password_confirm" type="password" class="form-control form-password rounded-pill {{ $errors->has('password_confirm') ? ' is-invalid' : '' }}" name="password_confirmation" required>
                                                    @if ($errors->has('password_confirm'))
                                                        <div class="invalid-feedback d-block">
                                                            {{ $errors->first('password_confirm') }}
                                                        </div>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                                            <label for="phone" class="font-weight-bold">Numéro de téléphone (Facultatif)</label>
                                            <input id="phone" type="tel" class="form-control rounded-pill {{ $errors->has('phone') ? ' is-invalid' : '' }}" name="phone" value="{{ old('phone') }}" pattern="^((\+\d{1,3}(-| )?\(?\d\)?(-| )?\d{1,5})|(\(?\d{2,6}\)?))(-| )?(\d{3,4})(-| )?(\d{4})(( x| ext)\d{1,5}){0,1}$">

                                            @if ($errors->has('phone'))
                                                <div class="invalid-feedback d-block">
                                                    {{ $errors->first('phone') }}
                                                </div>
                                            @endif
                                        </div>

                                        <div class="form-group">
                                            <div class="custom-control custom-checkbox my-3">
                                                <input type="checkbox" class="custom-control-input" name="type" id="type" value="responsable">
                                                <label class="custom-control-label font-weight-bold" for="type">Je suis un responsable</label>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <button type="submit" class="btn btn-lg btn-primary btn-pill btn-block text-uppercase font-weight-bold mb-2">
                                                Inscription *
                                            </button>
                                        </div>

                                        <p>
                                            <small>*En vous inscrivant, vous acceptez nos Conditions d'utilisation, notre Politique de confidentialité. Vous confirmez également que vous avez plus de 16 ans.</small>
                                        </p>
                                    </form>

                                    <div class="border-top my-4"></div>

                                    <div class="d-lg-flex align-items-center">
                                        <div class="col-12 col-lg">
                                            <a href="{{ url('login/facebook') }}" class="btn btn-primary btn-pill font-weight-bold">S'enregistrer avec Facebook *</a>
                                        </div>
                                        <div class="col col-lg mt-3 mt-lg-0">
                                            <a href="{{ route('login') }}" class="d-block text-center font-weight-bold">Vous avez déjà un compte ?</a>
                                        </div>
                                    </div>

                                    <div class="col col-lg-12 text-center mt-5">
                                        <a href="{{ url('/') }}" class="btn btn-outline-success btn-pill"><i class="fas fa-chevron-left mr-2"></i>Revenir à l'accueil</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="d-none d-md-flex col-md-4 col-lg-6 bg-register"></div>
            </div>
        </div>

        <!-- Scripts -->
        <script src="{{ asset('js/jquery.min.js') }}"></script>
        <script src="{{ asset('js/index.js') }}"></script>
    </body>
</html>

