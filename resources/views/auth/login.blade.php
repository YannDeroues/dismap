<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'Dismap') }}</title>

        <link rel="icon" type="image/png" href="{{ asset('img/logo_s.png') }}">

        <!-- Styles -->
        <link href="{{ asset('bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
        <link rel="stylesheet" href="{{ asset('css/fontawesome.min.css') }}">
        <link href="{{ asset('css/main.css') }}" rel="stylesheet">
    </head>

    <body>
        <div class="container-fluid bg-color-auth">
            <div class="row">
                <div class="col-md-8 col-lg-6">
                    <div class="min-vh-100 d-flex align-items-center py-5">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-md-9 col-lg-8 mx-auto">
                                    <form method="POST" action="{{ route('login') }}" class="needs-validation" novalidate>

                                        {{ csrf_field() }}

                                        <div class="form-group" >
                                            <label for="email" class="font-weight-bold">Adresse email*</label>
                                            <input id="email" type="email" class="form-control rounded-pill {{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>

                                            @if ($errors->has('email'))
                                                <div class="invalid-feedback d-block">
                                                    {{ $errors->first('email') }}
                                                </div>
                                            @endif
                                        </div>
                                        <div class="form-group">
                                            <label for="password" class="font-weight-bold">Mot de passe*</label>
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span id="showPwd" class="btn btn-pill btn-primary">
                                                        <i class="far fa-eye" id="iconPwd"></i>
                                                    </span>
                                                </div>
                                                <input type="password" class="form-control form-password rounded-pill {{ $errors->has('password') ? ' is-invalid' : '' }}" id="password" name="password" data-toggle="password" required>

                                                @if ($errors->has('password'))
                                                    <div class="invalid-feedback d-block">
                                                        {{ $errors->first('password') }}
                                                    </div>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="custom-control custom-checkbox mb-3">
                                                <input type="checkbox" class="custom-control-input" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                                <label class="custom-control-label font-weight-bold" for="remember">Se souvenir de moi</label>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <button type="submit" class="btn btn-lg btn-primary btn-pill btn-block text-uppercase font-weight-bold mb-2">
                                                Connexion
                                            </button>
                                        </div>

                                        <div class="text-center">
                                            <a class="btn btn-link" href="{{ route('password.request') }}">
                                                Mot de passe oublié ?
                                            </a>
                                        </div>
                                    </form>

                                    <div class="border-top my-4"></div>

                                    <div class="d-lg-flex align-items-center">
                                        <div class="col-12 col-lg">
                                            <a href="{{ url('login/facebook') }}" class="btn btn-primary btn-pill font-weight-bold">Se connecter avec Facebook</a>
                                        </div>
                                        <div class="col col-lg mt-3 mt-lg-0">
                                            <a href="{{ route('register') }}" class="d-block text-center font-weight-bold">Vous n'avez pas de compte ?</a>
                                        </div>
                                    </div>

                                    <div class="col col-lg-12 text-center mt-5">
                                        <a href="{{ url('/') }}" class="btn btn-outline-success btn-pill"><i class="fas fa-chevron-left mr-2"></i>Revenir à l'accueil</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="d-none d-md-flex col-md-4 col-lg-6 bg-login"></div>
            </div>
        </div>

        <!-- Scripts -->
        <script src="{{ asset('js/jquery.min.js') }}"></script>
        <script src="{{ asset('js/index.js') }}"></script>
    </body>
</html>
