@extends('layouts.app')

@section('content')

    @include('layouts.navbar')

    <div class="container my-5">
        <div class="row justify-content-center text-center">
            <div class="col-md-12">

                <h1 class="mb-5">Réinitialiser mon mot de passe</h1>

                @if (session('status'))
                    <div class="alert alert-success rounded-pill">
                        {{ session('status') }}
                    </div>
                @endif

                <form method="POST" action="{{ route('password.email') }}">

                    {{ csrf_field() }}

                    <div class="col-md-6 offset-md-3">
                        <div class="form-group">
                            <label for="email" class="text-left font-weight-bold">Adresse email</label>
                            <div class="input-group">
                                <input name="email" type="email" class="form-control border-primary" placeholder="exemple@exemple.com" aria-label="Adresse email" id="email" aria-describedby="button-validate" value="{{ old('email') }}" required>
                                <div class="input-group-append">
                                    <button type="submit" class="btn btn-outline-primary btn-pill font-weight-bold text-uppercase" id="button-validate">Envoyer</button>
                                </div>
                            </div>
                            @if ($errors->has('email'))
                                <div class="invalid-feedback d-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </div>
                            @endif
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
