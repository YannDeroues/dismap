@extends('layouts.app')

@section('content')

    @include('layouts.navbar')

    <div class="container my-5">
        <div class="row justify-content-center">
            <div class="col-md-12">

                <h1 class="mb-5">Réinitialiser mon mot de passe</h1>

                <div class="card">
                    <div class="card-body">
                        <form method="POST" action="{{ route('password.request') }}">

                            {{ csrf_field() }}

                            <input type="hidden" name="token" value="{{ $token }}">

                            <div class="form-group">
                                <label for="email" class="font-weight-bold">Email</label>
                                <input id="email" type="email" class="form-control rounded-pill" name="email" value="{{ $email or old('email') }}" required autofocus>
                                @if ($errors->has('email'))
                                    <div class="invalid-feedback d-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </div>
                                @endif
                            </div>

                            <div class="form-row">
                                <div class="form-group col-12 col-md">
                                    <label for="password" class="font-weight-bold">Nouveau mot de passe</label>
                                    <input id="password" type="password" class="form-control rounded-pill" name="password" required>
                                    @if ($errors->has('password'))
                                        <div class="invalid-feedback d-block">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </div>
                                    @endif
                                </div>

                                <div class="form-group col col-md">
                                    <label for="password-confirm" class="font-weight-bold">Confirmation</label>
                                    <input id="password-confirm" type="password" class="form-control rounded-pill" name="password_confirmation" required>
                                    @if ($errors->has('password_confirmation'))
                                        <div class="invalid-feedback d-block">
                                            <strong>{{ $errors->first('password_confirmation') }}</strong>
                                        </div>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group mt-4 d-flex justify-content-center">
                                <button type="submit" class="btn btn-primary btn-pill text-uppercase">
                                    Valider
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
