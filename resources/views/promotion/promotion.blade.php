@extends('layouts.app')

@section('content')

    @include('layouts.navbar')

    <div class="container mt-5">
        <div class="row">
            <div class="col-lg-12">
                <div class="card mb-3">
                    <div class="row no-gutters">
                        <div class="col-md-12 col-lg-8">
                            <div id="carouselExampleIndicators" class="carousel slide shadow-none" data-ride="carousel">
                                <ol class="carousel-indicators">
                                    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                                    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                                </ol>
                                <div class="carousel-inner rounded">
                                    <div class="carousel-item active">
                                        <img class="d-block" src="{{ asset('uploads/promotions/'.$promo->photo1_promo) }}" alt="Illustration 1 de la promotion">
                                    </div>

                                    @if ($promo->photo2_promo)
                                        <div class="carousel-item">
                                            <img class="d-block" src="{{ asset('uploads/promotions/'.$promo->photo2_promo) }}" alt="Illustration 2 de la promotion">
                                        </div>
                                    @endif

                                    @if ($promo->photo3_promo)
                                        <div class="carousel-item">
                                            <img class="d-block" src="{{ asset('uploads/promotions/'.$promo->photo3_promo) }}" alt="Illustration 3 de la promotion">
                                        </div>
                                    @endif
                                </div>
                                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span>
                                </a>
                                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Next</span>
                                </a>
                            </div>
                        </div>
                        <div class="col-md col-lg">
                            <div class="card-body">
                                <h4 class="card-title mb-4">{{ $shop->nom_mag }}</h4>
                                <p class="lead">
                                    <i class="fas fa-bullhorn mr-2"></i>{{ $promo->libelle_promo }}
                                </p>
                                <div class="row">
                                    @if ($temps->invert)
                                        <div class="col-12 mb-3">
                                            <h3>La promotion est terminée</h3>
                                        </div>
                                    @else
                                        <div class="col-12 mb-3">
                                            <form method="GET" action="{{ route('adhesion_promo', ['id_promo' => $promo->id]) }}">
                                                <button type="submit" class="btn btn-outline-secondary btn-block btn-pill btn-lg mb-3">{{ $promo->code_promo }}</button>
                                            </form>
                                            @if(session()->has('message'))
                                                <div class="alert alert-success rounded-pill alert-dismissible fade show" role="alert">
                                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                    <strong>
                                                        <i class="fas fa-check mr-2"></i>{{ session()->get('message') }}
                                                    </strong>
                                                </div>
                                            @endif
                                            <div class="progress-wrapper">
                                                <span class="progress-label">Expire le: {{ $promo->date_fin_promo }}</span>
                                                <?php $colorBar = ''; ?>
                                                @if($temps->days < 5)
                                                    <?php $colorBar = 'bg-primary'; ?>
                                                @elseif($temps->days < 10)
                                                    <?php $colorBar = 'bg-warning'; ?>
                                                @else
                                                    <?php $colorBar = 'bg-danger'; ?>
                                                @endif
                                                <div class="progress progress-lg my-3">
                                                    <div class="progress-bar {{ $colorBar }}" role="progressbar" aria-valuenow="{{ ($temps->days*10) }}" aria-valuemin="0" aria-valuemax="100" style="width: {{ ($temps->days*10) }}%;">
                                                        <span class="progress-value">{{ $temps->days }} jour(s) restant(s)</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                    <div class="col">
                                        <div class="card-icon-text">
                                            <span class="mx-3"><i class="fas fa-map-pin"></i></span>
                                            <p>{{ $shop->ad1_mag }}</p>
                                        </div>
                                        <div class="card-icon-text">
                                            <span class="mx-3"><i class="fas fa-phone"></i></span>
                                            <p>{{ $shop->tel_mag }}</p>
                                        </div>
                                        <div class="card-icon-text">
                                            <span class="mx-3"><i class="fas fa-at"></i></span>
                                            <p>{{ $shop->mail_mag }}</p>
                                        </div>
                                        <div class="card-icon-text">
                                            <span class="mx-3"><i class="fas fa-star"></i></span>
                                            <p>4.5 | <a href="#">25 avis</a></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-12">
                @if(session()->has('messageCodeAvis'))
                    <div class="alert alert-danger rounded-pill alert-dismissible fade show" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <strong>
                            <i class="fas fa-exclamation mr-2"></i>{{ session()->get('messageCodeAvis') }}
                        </strong>
                    </div>
                @endif
                @if(session()->has('messageAvis'))
                    <div class="alert alert-success rounded-pill alert-dismissible fade show" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <strong>
                            <i class="fas fa-check mr-2"></i>{{ session()->get('messageAvis') }}
                        </strong>
                    </div>
                @endif
                <div class="row mt-4">
                    <div class="col-md-12">
                        <div class="card mb-4">
                            <div class="card-body">
                                <form action="{{ route('adhesion_commentaire_post', ['id_promo' => $promo->id]) }}" method="POST">

                                    {{ csrf_field() }}

                                    <div class="form-row">
                                        <div class="form-group col-12 col-md">
                                            <label for="commentaire_avis" class="font-weight-bold">Commentaire</label>
                                            <textarea name="commentaire_avis" id="commentaire_avis" class="form-control" placeholder="Votre commentaire" cols="30" rows="10"></textarea>
                                        </div>
                                        <div class="form-group col-12 col-md-2 pl-md-4">
                                            <div>
                                                <label for="code_avis_promo" class="font-weight-bold">Code avis*</label>
                                                <input type="text" maxlength="3" name="code_avis_promo" id="code_avis_promo" class="form-control" placeholder="Exemple: 123" required>
                                            </div>
                                            <div class="my-4">
                                                <label for="note_avis" class="font-weight-bold">Note*</label>
                                                <select id="note_avis" name="note_avis">
                                                    <option value="1">1</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                    <option value="5">5</option>
                                                </select>
                                            </div>
                                            <button class="btn btn-primary btn-block btn-pill text-uppercase font-weight-bold" type="submit">Publier</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    @if(isset($commentaires))
                        @foreach($commentaires as $commentaire)
                            @if(!empty($commentaire->note_avis))
                                <div class="col-12 col-md-6">
                                    <div class="details--container">
                                        <div class="card">
                                            <div class="card-body">
                                                <div class="row">
                                                    <div class="col-md">
                                                        <h4>{{ $commentaire->firstname }} <span class="text-uppercase">{{ $commentaire->lastname }}</span></h4>
                                                    </div>
                                                    <div class="col-md-5 mb-2 mb-lg-0 text-md-right details--container-stars">
                                                        @switch($commentaire->note_avis)
                                                            @case(1)
                                                                <i class="fas fa-star"></i>
                                                                <i class="far fa-star"></i>
                                                                <i class="far fa-star"></i>
                                                                <i class="far fa-star"></i>
                                                                <i class="far fa-star"></i>
                                                            @break
                                                            @case(2)
                                                                <i class="fas fa-star"></i>
                                                                <i class="fas fa-star"></i>
                                                                <i class="far fa-star"></i>
                                                                <i class="far fa-star"></i>
                                                                <i class="far fa-star"></i>
                                                            @break
                                                            @case(3)
                                                                <i class="fas fa-star"></i>
                                                                <i class="fas fa-star"></i>
                                                                <i class="fas fa-star"></i>
                                                                <i class="far fa-star"></i>
                                                                <i class="far fa-star"></i>
                                                            @break
                                                            @case(4)
                                                                <i class="fas fa-star"></i>
                                                                <i class="fas fa-star"></i>
                                                                <i class="fas fa-star"></i>
                                                                <i class="fas fa-star"></i>
                                                                <i class="far fa-star"></i>
                                                            @break
                                                            @case(5)
                                                                <i class="fas fa-star"></i>
                                                                <i class="fas fa-star"></i>
                                                                <i class="fas fa-star"></i>
                                                                <i class="fas fa-star"></i>
                                                                <i class="fas fa-star"></i>
                                                            @break
                                                            @default
                                                            <span>Erreur !</span>
                                                        @endswitch
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col">
                                                        <p class="mb-3">{{ $commentaire->commentaire_avis }}</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        @endforeach
                    @else
                        <div class="col">
                            <h3>Il n'y a pas de commentaire sur cette promotion</h3>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>

    @include('layouts.footer')
    @include('layouts._js')
    <link rel="stylesheet" href="{{ asset('css/fontawesome-stars.css') }}">
    <script src="{{ asset('js/jquery.barrating.min.js') }}"></script>
    <script src="{{ asset('js/') }}"></script>
    <script>
        $(function() {
            $('#note_avis').barrating({
                theme: 'fontawesome-stars'
            });
        });
    </script>

@endsection
