@extends('layouts.app')

@section('content')

    @include('layouts.navbar')

    <div class="container">
        <div class="row">
            <div class="col mt-5">

                @if(session()->has('message'))
                    <div class="alert alert-success rounded-pill alert-dismissible fade show" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <strong><i class="fas fa-check mr-2"></i>{{ session()->get('message') }}</strong>
                    </div>
                @endif

                <div class="card">
                    <div class="card-body">

                        <h3 class="mb-4">Promotion du magasin "{{ $shop->nom_mag }}"</h3>

                        <form enctype="multipart/form-data" method="post" action="{{ route('edit_promo_post', ['id_promo' => $promo->id]) }}">

                            {{ csrf_field() }}

                            <input type="hidden" name="id_mag" value="{{ $shop->id }}">

                            <div class="form-row">
                                <div class="form-group col-12 col-md-10">
                                    <label for="libelle_promo" class="font-weight-bold">Libellé*</label>
                                    <input type="text" class="form-control rounded-pill" name="libelle_promo" id="libelle_promo" value="{{ old('libelle_promo', $promo->libelle_promo) }}" required>
                                    @if ($errors->has('libelle_promo'))
                                        <div class="invalid-feedback d-block">
                                            {{ $errors->first('libelle_promo') }}
                                        </div>
                                    @endif
                                </div>
                                <div class="form-group col col-md">
                                    <label for="etat_promo" class="text-capitalize font-weight-bold">état*</label>
                                    <div class="custom-control custom-toggle">
                                        <input type="checkbox" id="etat_promo" name="etat_promo" class="custom-control-input" {{ $promo->etat_promo ? "checked" : "" }}>
                                        <label class="custom-control-label text-capitalize font-weight-bold" id="statusText" for="etat_promo">{{ $promo->etat_promo ? "Activée" : "Désactivée" }}</label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="form-group col-12 col-md">
                                    <label for="photo1_promo" class="font-weight-bold">Photo 1</label>
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" id="photo1_promo"
                                               name="photo1_promo">
                                        <label class="custom-file-label rounded-pill" for="photo1_promo">Photo 1</label>
                                    </div>
                                    @if ($errors->has('photo1_promo'))
                                        <div class="invalid-feedback d-block">
                                            {{ $errors->first('photo1_promo') }}
                                        </div>
                                    @endif
                                </div>

                                <div class="form-group col-12 col-md">
                                    <label for="photo1_promo" class="font-weight-bold">Photo 2</label>
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" id="photo2_promo"
                                               name="photo2_promo">
                                        <label class="custom-file-label rounded-pill" for="photo2_promo">Photo 2</label>
                                    </div>
                                    @if ($errors->has('photo2_promo'))
                                        <div class="invalid-feedback d-block">
                                            {{ $errors->first('photo2_promo') }}
                                        </div>
                                    @endif
                                </div>

                                <div class="form-group col col-md">
                                    <label for="photo1_promo" class="font-weight-bold">Photo 3</label>
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" id="photo3_promo"
                                               name="photo3_promo">
                                        <label class="custom-file-label rounded-pill" for="photo3_promo">Photo 3</label>
                                    </div>
                                    @if ($errors->has('photo3_promo'))
                                        <div class="invalid-feedback d-block">
                                            {{ $errors->first('photo3_promo') }}
                                        </div>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <a data-toggle="collapse" href="#photo_promo" role="button" aria-expanded="false"
                                   aria-controls="collapse_photo_promo" class="font-weight-bold">Voir l'image
                                    actuelle</a>
                                <div class="collapse mt-3" id="photo_promo">
                                    <img src="/uploads/promotions/{{ $promo->photo1_promo }}"
                                         alt="Image de la promotion" class="img-fluid rounded shadow">
                                </div>
                            </div>

                            <a href="{{ url('resp/shop/'.$shop->id) }}" class="btn btn-secondary btn-pill">Retour</a>
                            <button type="submit" class="btn btn-primary btn-pill">Editer</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('layouts._js')
    <script>
        $('#etat_promo').click(function () {
            $('#etat_promo').prop('checked') ? $('#statusText').text('Activée') : $('#statusText').text('Désactivée');
        });
    </script>
@endsection
