@extends('layouts.app')

@section('content')

    <body class="bg-gradient-red">

        @include('layouts.navbar')

        <div class="container my-5">

            <h1 class="text-center mb-5 text-white"><a href="{{ url('/') }}" class="mr-5 text-white"><i class="fas fa-arrow-circle-left"></i></a> Jules GAP</h1>

            <div class="row">
                <div class="col-12 col-lg">

                    <h3 class="text-uppercase text-center mb-5 text-white">Avis positifs</h3>

                    <div class="details--container">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md">
                                        <h4>Lorem <span class="text-uppercase">ipsum</span></h4>
                                    </div>
                                    <div class="col-md mb-2 mb-lg-0 text-md-right details--container-stars">
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci, aliquid cum delectus dolor dolorum earum ipsa ipsam ipsum molestiae molestias natus nulla placeat repellat sed sunt suscipit, vitae voluptas voluptatibus.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="details--container">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md">
                                        <h4>Lorem <span class="text-uppercase">ipsum</span></h4>
                                    </div>
                                    <div class="col-md mb-2 mb-lg-0 text-md-right details--container-stars">
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci, aliquid cum delectus dolor dolorum earum ipsa ipsam ipsum molestiae molestias natus nulla placeat repellat sed sunt suscipit, vitae voluptas voluptatibus.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="details--container">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md">
                                        <h4>Lorem <span class="text-uppercase">ipsum</span></h4>
                                    </div>
                                    <div class="col-md mb-2 mb-lg-0 text-md-right details--container-stars">
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="far fa-star"></i>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci, aliquid cum delectus dolor dolorum earum ipsa ipsam ipsum molestiae molestias natus nulla placeat repellat sed sunt suscipit, vitae voluptas voluptatibus.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="details--container">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md">
                                        <h4>Lorem <span class="text-uppercase">ipsum</span></h4>
                                    </div>
                                    <div class="col-md mb-2 mb-lg-0 text-md-right details--container-stars">
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="far fa-star"></i>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci, aliquid cum delectus dolor dolorum earum ipsa ipsam ipsum molestiae molestias natus nulla placeat repellat sed sunt suscipit, vitae voluptas voluptatibus.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <nav aria-label="Avis positives" class="d-flex justify-content-center">
                        <ul class="pagination">
                            <li class="page-item">
                                <a class="page-link" href="#" aria-label="Previous">
                                    <span aria-hidden="true">&laquo;</span>
                                </a>
                            </li>
                            <li class="page-item"><a class="page-link" href="#">1</a></li>
                            <li class="page-item"><a class="page-link" href="#">2</a></li>
                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                            <li class="page-item">
                                <a class="page-link" href="#" aria-label="Next">
                                    <span aria-hidden="true">&raquo;</span>
                                </a>
                            </li>
                        </ul>
                    </nav>
                </div>

                <div class="col col-lg mt-5 mt-md-0">
                    <h3 class="text-uppercase text-center mb-5 text-white">Avis négatifs</h3>

                    <div class="details--container">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md">
                                        <h4>Lorem <span class="text-uppercase">ipsum</span></h4>
                                    </div>
                                    <div class="col-md mb-2 mb-lg-0 text-md-right details--container-stars">
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="far fa-star"></i>
                                        <i class="far fa-star"></i>
                                        <i class="far fa-star"></i>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci, aliquid cum delectus dolor dolorum earum ipsa ipsam ipsum molestiae molestias natus nulla placeat repellat sed sunt suscipit, vitae voluptas voluptatibus.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="details--container">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md">
                                        <h4>Lorem <span class="text-uppercase">ipsum</span></h4>
                                    </div>
                                    <div class="col-md mb-2 mb-lg-0 text-md-right details--container-stars">
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="far fa-star"></i>
                                        <i class="far fa-star"></i>
                                        <i class="far fa-star"></i>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci, aliquid cum delectus dolor dolorum earum ipsa ipsam ipsum molestiae molestias natus nulla placeat repellat sed sunt suscipit, vitae voluptas voluptatibus.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="details--container">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md">
                                        <h4>Lorem <span class="text-uppercase">ipsum</span></h4>
                                    </div>
                                    <div class="col-md mb-2 mb-lg-0 text-md-right details--container-stars">
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="far fa-star"></i>
                                        <i class="far fa-star"></i>
                                        <i class="far fa-star"></i>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci, aliquid cum delectus dolor dolorum earum ipsa ipsam ipsum molestiae molestias natus nulla placeat repellat sed sunt suscipit, vitae voluptas voluptatibus.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="details--container">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md">
                                        <h4>Lorem <span class="text-uppercase">ipsum</span></h4>
                                    </div>
                                    <div class="col-md mb-2 mb-lg-0 text-md-right details--container-stars">
                                        <i class="fas fa-star"></i>
                                        <i class="far fa-star"></i>
                                        <i class="far fa-star"></i>
                                        <i class="far fa-star"></i>
                                        <i class="far fa-star"></i>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci, aliquid cum delectus dolor dolorum earum ipsa ipsam ipsum molestiae molestias natus nulla placeat repellat sed sunt suscipit, vitae voluptas voluptatibus.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <nav aria-label="Avis négatives" class="d-flex justify-content-center">
                        <ul class="pagination">
                            <li class="page-item">
                                <a class="page-link" href="#" aria-label="Previous">
                                    <span aria-hidden="true">&laquo;</span>
                                </a>
                            </li>
                            <li class="page-item"><a class="page-link" href="#">1</a></li>
                            <li class="page-item"><a class="page-link" href="#">2</a></li>
                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                            <li class="page-item">
                                <a class="page-link" href="#" aria-label="Next">
                                    <span aria-hidden="true">&raquo;</span>
                                </a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>

        @include('layouts._js')

@endsection
