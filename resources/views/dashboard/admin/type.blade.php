@extends('layouts.app')

@section('content')

    @include('layouts.navbar')

    <div class="container mt-5">

        @if(session()->has('messageCat'))
            <div class="alert alert-success rounded-pill alert-dismissible fade show" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <strong>
                    <i class="fas fa-check mr-2"></i>{{ session()->get('messageCat') }}
                </strong>
            </div>
        @endif

        <div class="card mb-5">
            <div class="card-body">

                <h4 class="card-title">
                    Insertion d'une catégorie pour "{{ $type->libelle_type }}"
                    <span>
                        <a href="{{ route('edit_type', ['id_type' => $type->id]) }}" title="Modifier la catégorie {{ $type->libelle_type }}"><i class="fas fa-edit"></i></a>
                    </span>
                </h4>


                <form method="post" action="{{ route('add_cat_post', ['id_type' => $type->id]) }}">

                    {{ csrf_field() }}

                    <input type="hidden" name="id_type" value="{{ $type->id }}">

                    <label for="libelle_cat" class="font-weight-bold">Catégorie*</label>
                    <div class="input-group mb-3">
                        <input type="text" class="form-control" name="libelle_cat" id="libelle_cat" placeholder="Indien, Chinois, ..." value="{{ old('libelle_cat') }}" required>
                        <div class="input-group-append">
                            <button type="submit" class="btn btn-primary btn-pill font-weight-bold">Valider</button>
                        </div>
                    </div>
                    @if ($errors->has('libelle_cat'))
                        <div class="invalid-feedback d-block">
                            {{ $errors->first('libelle_cat') }}
                        </div>
                    @endif
                </form>
            </div>
        </div>

        <h1 class="mb-4">Liste des catégories associés</h1>

        <ul class="list-group">
            @foreach ($cats as $cat)
                <li class="list-group-item list-group-item-action text-capitalize">{{ $cat->libelle_cat }}</li>
            @endforeach
        </ul>
    </div>

    @include('layouts._js')

@endsection