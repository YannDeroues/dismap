@extends('layouts.app')

@section('content')

    @include('layouts.navbar')

    <div class="container mt-5">

        <h1 class="mb-4">Administration</h1>

        @if(session()->has('messageType'))
            <div class="alert alert-success rounded-pill alert-dismissible fade show" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <strong>
                    <i class="fas fa-check mr-2"></i>{{ session()->get('messageType') }}
                </strong>
            </div>
        @endif

        <div class="card mb-5">
            <div class="card-body">

                <h4 class="card-title">Insertion d'un nouveau type</h4>

                <form method="post" action="{{ route('add_type_post') }}">

                    {{ csrf_field() }}

                    <label for="libelle_type" class="font-weight-bold">Type*</label>
                    <div class="input-group mb-3">
                        <input type="text" class="form-control" name="libelle_type" id="libelle_type" placeholder="Librairie, Restaurant..." value="{{ old('libelle_type') }}" required>
                        <div class="input-group-append">
                            <button type="submit" class="btn btn-primary btn-pill font-weight-bold">Valider</button>
                        </div>
                    </div>
                    @if ($errors->has('libelle_type'))
                        <div class="invalid-feedback d-block">
                            {{ $errors->first('libelle_type') }}
                        </div>
                    @endif
                </form>
            </div>
        </div>

        <h1 class="mb-4">Liste des types</h1>

        <div class="list-group">
            @foreach ($types as $type)
                <a href="{{ route('display_type', ['id_type' => $type->id]) }}" class="list-group-item list-group-item-action text-capitalize">{{ $type->libelle_type }}</a>
            @endforeach
        </div>
    </div>

    @include('layouts._js')

@endsection