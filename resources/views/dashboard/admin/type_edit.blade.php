@extends('layouts.app')

@section('content')

    @include('layouts.navbar')

        <div class="container mt-5">

            <h1 class="mb-4">Edition de {{ $type->libelle_type }}</h1>

            @if(session()->has('messageType'))
                <div class="alert alert-success rounded-pill alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <strong>
                        <i class="fas fa-check mr-2"></i>{{ session()->get('messageType') }}
                    </strong>
                </div>
            @endif

            <div class="card mb-5">
                <div class="card-body">

                    <form method="post" action="{{ route('edit_type_post', ['id_type' => $type->id]) }}">

                        {{ csrf_field() }}

                        <label for="libelle_type" class="font-weight-bold">Nouveau nom du type*</label>
                        <div class="input-group mb-3">
                            <input type="text" class="form-control" name="libelle_type" id="libelle_type" placeholder="Librairie, Restaurant..." value="{{ old('libelle_type', $type->libelle_type) }}" required>
                            <div class="input-group-append">
                                <button type="submit" class="btn btn-primary btn-pill font-weight-bold">Valider</button>
                            </div>
                        </div>
                        @if ($errors->has('libelle_type'))
                            <div class="invalid-feedback d-block">
                                {{ $errors->first('libelle_type') }}
                            </div>
                        @endif
                    </form>
                </div>
            </div>

            <div class="d-flex justify-content-center">
                <a href="{{ route('display_type', ['id_type' => $type->id]) }}" class="btn btn-info btn-lg btn-pill text-uppercase"><i class="fas fa-chevron-left mr-2"></i>Retour</a>
            </div>
        </div>
    
    @include('layouts._js')

@endsection