@extends('layouts.app')

@section('content')

    <body>

        @include('layouts.navbar')

        <div class="container my-5">
            <div class="row">
                <div class="col mb-4">
                    <div class="card">
                        <div class="card-body p-5">
                            <h4 class="card-title mb-4 text-uppercase">Mes informations</h4>

                            <form method="post" action="{{ route('user_edit_post') }}" class="needs-validation" novalidate>

                                {{csrf_field()}}

                                <input type="hidden" name="user_id" value="{{$user->id}}" required>

                                <div class="form-row">
                                    <div class="col-12 col-lg form-group">
                                        <label for="firstname" class="font-weight-bold">Prénom*</label>
                                        <input type="text" class="form-control rounded-pill {{ $errors->has('firstname') ? ' is-invalid' : '' }}" id="firstname" name="firstname" value="{{old('firstname', $user->firstname)}}" required>
                                        @if ($errors->has('firstname'))
                                            <div class="invalid-feedback d-block">
                                                {{ $errors->first('firstname') }}
                                            </div>
                                        @endif
                                    </div>

                                    <div class="col col-lg form-group">
                                        <label for="lastname" class="font-weight-bold">Nom*</label>
                                        <input type="text" class="form-control rounded-pill {{ $errors->has('lastname') ? ' is-invalid' : '' }}" id="lastname" name="lastname" value="{{old('lastname', $user->lastname)}}" required>
                                        @if ($errors->has('lastname'))
                                            <div class="invalid-feedback d-block">
                                                {{ $errors->first('lastname') }}
                                            </div>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-row">
                                    <div class="form-group col-12 col-md">
                                        <label for="email" class="font-weight-bold">Email*</label>
                                        <input type="email" class="form-control rounded-pill {{ $errors->has('email') ? ' is-invalid' : '' }}" id="email" name="email" value="{{old('email', $user->email)}}" required>
                                        @if ($errors->has('email'))
                                            <div class="invalid-feedback d-block">
                                                {{ $errors->first('email') }}
                                            </div>
                                        @endif
                                    </div>
                                    <div class="form-group col col-md">
                                        <label for="phone" class="font-weight-bold">Numéro de téléphone</label>
                                        <input type="tel" class="form-control rounded-pill {{ $errors->has('phone') ? ' is-invalid' : '' }}" id="phone" name="phone" pattern="^((\+\d{1,3}(-| )?\(?\d\)?(-| )?\d{1,5})|(\(?\d{2,6}\)?))(-| )?(\d{3,4})(-| )?(\d{4})(( x| ext)\d{1,5}){0,1}$" value="{{old('phone', $user->phone)}}">
                                        @if ($errors->has('phone'))
                                            <div class="invalid-feedback d-block">
                                                {{ $errors->first('phone') }}
                                            </div>
                                        @endif
                                    </div>
                                </div>

                                <div class="col-md-2 offset-md-5 mt-3">
                                    <button type="submit" class="btn btn-pill btn-primary btn-block font-weight-bold">Valider</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-12 mb-4">
                    <a href="#" data-toggle="modal" data-target="#changePassword" class="card text-decoration-none">
                        <div class="card-body bg-primary rounded">
                            <div class="row justify-content-md-between align-items-md-center">
                                <div class="col-12 col-md-10">
                                    <h4 class="text-white text-uppercase mb-4 m-md-0">Modifier mon mot de passe</h4>
                                </div>
                                <div class="col col-md text-md-right">
                                    <i class="fas fa-key text-white d-inline" style="font-size: 40px"></i>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-12 mb-4">
                    <a href="#" class="card text-decoration-none">
                        <div class="card-body bg-info rounded">
                            <div class="row justify-content-md-between align-items-md-center">
                                <div class="col-12 col-md-10">
                                    <h4 class="text-white text-uppercase mb-4 m-md-0">Exporter mes données</h4>
                                </div>
                                <div class="col col-md text-md-right">
                                    <i class="fas fa-database text-white d-inline" style="font-size: 40px"></i>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col">
                    <div class="card">
                        <div class="card-body bg-danger rounded">
                            <div class="row justify-content-md-between align-items-md-center">
                                <div class="col-12 col-md-4">
                                    <h4 class="text-white font-weight-bold text-uppercase mb-4 m-md-0">Zone sensible</h4>
                                </div>
                                <div class="col col-md text-md-right">
                                    <a href="#" class="btn btn-outline-white text-uppercase font-weight-bolder btn-pill btn-lg"><i class="fas fa-exclamation-triangle mr-3"></i>Supprimer mon compte</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="changePassword" tabindex="-1" role="dialog" aria-labelledby="changePassword" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                <div class="modal-content">
                    <form method="post" action="{{ route('password_edit') }}" class="needs-validation" novalidate>
                        <div class="modal-header">
                            <h5 class="modal-title" id="changePasswordTitle">Modifier mon mot de passe</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">

                            {{csrf_field()}}

                            <input type="hidden" name="user_id" value="{{$user->id}}" required>

                            <div class="form-group">
                                <label for="current_password" class="font-weight-bold">Mot de passe actuel*</label>
                                <input type="password" class="form-control rounded-pill {{ $errors->has('current_password') ? ' is-invalid' : '' }}" name="current_password" autofocus required>
                                @if ($errors->has('current_password'))
                                    <div class="invalid-feedback d-block">
                                        {{ $errors->first('current_password') }}
                                    </div>
                                @endif
                            </div>
                            <div class="form-row">
                                <div class="form-group col-12 col-lg">
                                    <label for="password" class="font-weight-bold">Nouveau mot de passe*</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span id="showPwd" class="btn btn-pill btn-primary">
                                                <i class="far fa-eye" id="iconPwd"></i>
                                            </span>
                                        </div>
                                        <input type="password" id="password" class="form-control rounded-pill form-password {{ $errors->has('password') ? ' is-invalid' : '' }}"  name="password" required>

                                        @if ($errors->has('password'))
                                            <div class="invalid-feedback d-block">
                                                {{ $errors->first('password') }}
                                            </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group col col-lg">
                                    <label for="password_confirm" class="font-weight-bold">Confirmer votre mot de passe*</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span id="showPwdConf" class="btn btn-pill btn-primary">
                                                <i class="far fa-eye" id="iconPwdConf"></i>
                                            </span>
                                        </div>
                                        <input type="password" id="password_confirm" class="form-control rounded-pill form-password {{ $errors->has('password') ? ' is-invalid' : '' }}" name="password_confirmation" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary btn-pill" data-dismiss="modal">Fermer</button>
                            <button type="submit" class="btn btn-primary btn-pill">Valider</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        @include('layouts._js')
        <script>
            (function() {
                'use strict';
                window.addEventListener('load', function() {
                    const forms = document.getElementsByClassName('needs-validation');
                    const validation = Array.prototype.filter.call(forms, function(form) {
                        form.addEventListener('submit', function(event) {
                            if (form.checkValidity() === false) {
                                event.preventDefault();
                                event.stopPropagation();
                            }
                            form.classList.add('was-validated');
                        }, false);
                    });
                }, false);
            })();

            @if ($errors->has('password') || $errors->has('current_password') )
                $('#changePassword').modal('show');
            @endif
        </script>
@endsection