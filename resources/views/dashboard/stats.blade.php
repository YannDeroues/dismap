@extends('layouts.app')

@section('content')

    <body>

        @include('layouts.navbar')

        <div class="container my-5">
            <div class="row">
                <div class="col-12 col-md-4 mb-4">
                    <h3 class="mb-5">Statistiques</h3>
                    <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                        <a class="nav-link rounded-pill active" id="v-pills-1-tab" data-toggle="pill" href="#v-pills-1" role="tab" aria-controls="v-pills-1" aria-selected="true">Jules GAP</a>
                        <a class="nav-link rounded-pill" id="v-pills-2-tab" data-toggle="pill" href="#v-pills-2" role="tab" aria-controls="v-pills-2" aria-selected="false">Jules GRENOBLE</a>
                    </div>
                </div>
                <div class="col-12 col-md">
                    <div class="tab-content" id="v-pills-tabContent">
                        <div class="tab-pane fade show active" id="v-pills-1" role="tabpanel" aria-labelledby="v-pills-1-tab">
                            <div class="card mb-4">
                                <div class="card-body">
                                    <h4><i class="far fa-chart-bar mr-2"></i>Nombre de code utilisé pendant la durée d'activation de la promo</h4>
                                    <canvas id="chartCode"></canvas>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-body">
                                    <h4><i class="fas fa-chart-bar mr-2"></i>180 avis - Promo: -10 sur la nouvelle collection</h4>
                                    <div class="w-50 mx-auto">
                                        <canvas id="chartReviews" class="m-auto"></canvas>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="v-pills-2" role="tabpanel" aria-labelledby="v-pills-2-tab">
                            <p>Graphiques</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @include('layouts._js')
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>
        <script>
            $(document).ready(function() {
                const libData = ['01 Janv', '02 Janv', '03 Janv', '04 Janv', '05 Janv', '06 Janv', '07 Janv', '08 Janv', '09 Janv', '10 Janv', '11 Janv', '12 Janv', '13 Janv', '14 Janv', '15 Janv'];
                const config = {
                    type: 'line',
                    data: {
                        labels: libData,
                        datasets: [{
                            backgroundColor: 'rgba(255, 99, 132, 0.2)',
                            borderColor: 'rgba(255, 99, 132, 255)',
                            data: [12, 19, 3, 5, 20, 31, 0, 50, 3, 10, 71, 40, 164, 43, 19],
                            fill: false,
                        }]
                    },
                    options: {
                        responsive: true,
                        legend: {
                            display: false,
                            labels: {
                                display: false
                            }
                        },
                        title: {
                            display: true,
                            text: 'Promo: -10 sur la nouvelle collection'
                        },
                        tooltips: {
                            mode: 'index',
                            intersect: false,
                        },
                        hover: {
                            mode: 'nearest',
                            intersect: true
                        },
                        scales: {
                            xAxes: [{
                                display: true,
                                scaleLabel: {
                                    display: true,
                                    labelString: 'Durée totale de la promotion'
                                }
                            }],
                            yAxes: [{
                                display: true,
                                scaleLabel: {
                                    display: true,
                                    labelString: 'Nombre de code récupérés par les utilisateurs'
                                }
                            }]
                        }
                    }
                };

                const config2 = {
                    type: 'pie',
                    data: {
                        datasets: [{
                            data: [150, 30],
                            backgroundColor: ['#4E66F8','#adb5bd']
                        }],
                        labels: [
                            'Avis Positifs',
                            'Avis Négatifs'
                        ]
                    },
                    options: {
                        responsive: true
                    }
                };

                const ctx = document.getElementById('chartCode').getContext('2d');
                const ctx2 = document.getElementById('chartReviews').getContext('2d');
                window.myLine = new Chart(ctx, config);
                window.myLine = new Chart(ctx2, config2);
            });
        </script>
@endsection