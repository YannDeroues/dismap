@extends('layouts.app')

@section('content')

    @include('layouts.navbar')

    <div class="container my-5">
        <h2 class="font-weight-bold mb-5">Welcome, {{ Auth::user()->firstname }} !</h2>
        @if(Auth::user()->type !== "responsable")
            <div class="row">
                <div class="col">
                    <div class="card">
                        <div class="card-body">
                            <table id="table" class="table table-hover table-responsive-sm">
                                <thead>
                                <tr>
                                    <th scope="col">Promos</th>
                                    <th scope="col">Code</th>
                                    <th scope="col">Status</th>
                                    <th scope="col">Expire le</th>
                                    <th scope="col">Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($promos as $promo)
                                    <tr>
                                        <td>
                                            <a href="{{ route('get_promo', ['id_promo' => $promo->id_promo]) }}">
                                                {{ $promo->libelle_promo }}
                                            </a>
                                        </td>
                                        <td>{{ $promo->code_promo }}</td>
                                        <td data-order="{{ $promo->etat_promo ? 1 : 0 }}"><i class="fas fa-circle {{ $promo->etat_promo ? 'text-success' : 'text-danger' }}"></i></td>
                                        <td>{{ $promo->date_fin_promo }}</td>
                                        <td>
                                            <a href="{{route('delete_adhesion', ['id_adhesion' => $promo->id]) }}" title="Supprimer la promotion des favoris">
                                                <i class="fas fa-trash text-danger"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        @else
            <div class="row mb-md-2">
                <div class="col-12 col-md mb-4 mb-md-3">
                    <a href="{{ route('resp_shops') }}" class="card rounded-pill text-decoration-none">
                        <div class="row no-gutters dashboard--option">
                            <div class="col-4 d-flex justify-content-center align-items-center bg-primary rounded-pill py-4">
                                <i class="fa fa-shopping-bag fa-3x text-white"></i>
                            </div>
                            <div class="col-8 d-flex align-items-center ">
                                <h5 class="font-weight-bold mx-4 mb-0">Gestion magasins</h5>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col col-md mb-4 mb-md-3">
                    <a href="{{ url('stats') }}" class="card rounded-pill text-decoration-none">
                        <div class="row no-gutters dashboard--option">
                            <div class="col-4 d-flex justify-content-center align-items-center bg-primary rounded-pill py-4">
                                <i class="fa fa-chart-bar fa-3x text-white"></i>
                            </div>
                            <div class="col-8 d-flex align-items-center">
                                <h5 class="font-weight-bold mx-4 mb-0">Données analytiques</h5>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        @endif
    </div>

    @include('layouts._js')

    @if(Auth::user()->type !== "responsable")
        <script src="{{ asset('js/datatables.min.js') }}"></script>
        <script>
            $(document).ready(function() {
                $('#table').DataTable({
                    language: {
                        processing:     "Traitement en cours...",
                        search:         "Rechercher&nbsp;:",
                        lengthMenu:     "Afficher _MENU_ &eacute;l&eacute;ments",
                        info:           "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                        infoEmpty:      "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
                        infoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                        infoPostFix:    "",
                        loadingRecords: "Chargement en cours...",
                        zeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
                        emptyTable:     "Aucune donnée disponible dans le tableau",
                        paginate: {
                            first:      "Premier",
                            previous:   "Pr&eacute;c&eacute;dent",
                            next:       "Suivant",
                            last:       "Dernier"
                        },
                        aria: {
                            sortAscending:  ": activer pour trier la colonne par ordre croissant",
                            sortDescending: ": activer pour trier la colonne par ordre décroissant"
                        }
                    }
                });
            });
        </script>
    @endif
@endsection