<?php

use Illuminate\Database\Seeder;
use App\Ville;
use App\Type;
use App\Categorie;
class AllTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('villes')->insert([
            'nom_ville' => 'Gap',
            'cp_ville' => '05000',
            'code_insee_ville' => '05061',
            'long_ville' => '6.083333',
            'lat_ville' => '44.566667'
        ]);

        DB::table('villes')->insert([
            'nom_ville' => 'Le Puy en Velay',
            'cp_ville' => '43000',
            'code_insee_ville' => '43157',
            'long_ville' => '3.882936',
            'lat_ville' => '45.042768'
        ]);

        DB::table('types')->insert([
            'libelle_type' => 'restaurant'
        ]);

        DB::table('categories')->insert([
            'id_type' => '1',
            'libelle_cat' => 'indien'
        ]);
        
        DB::table('users')->insert([
            'firstname' => 'Responsable',
            'lastname' => 'GAP',
            'email' => 'resp@gmail.com',
            'password' => '$2y$10$SUqXKvHutnhs5jYfTQPCIu5R52qp9j0QhMSMqTBRHV/I3mgtKbIsK', // responsable
            'type' => 'responsable'
        ]);

        DB::table('users')->insert([
            'firstname' => 'Client',
            'lastname' => 'GAP',
            'email' => 'client@gmail.com',
            'password' => '$2y$10$SUqXKvHutnhs5jYfTQPCIu5R52qp9j0QhMSMqTBRHV/I3mgtKbIsK', // responsable
            'type' => 'client'
        ]);
    }
}
