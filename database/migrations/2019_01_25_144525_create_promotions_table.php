<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePromotionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('promotions', function (Blueprint $table) {
            $table->increments('id');

            $table->dateTime('date_debut_promo');
            $table->dateTime('date_fin_promo');
            $table->string('libelle_promo');
            $table->boolean('etat_promo');
            $table->string('code_promo');
            $table->string('code_avis_promo');
            $table->string('photo1_promo');
            $table->string('photo2_promo')->nullable();
            $table->string('photo3_promo')->nullable();

            $table->integer('id_mag')->unsigned();
            $table->foreign('id_mag')->references('id')->on('magasins');  
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('promotions');
    }
}
