<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMagasinsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('magasins', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nom_mag');
            $table->string('ad1_mag');
            $table->string('ad2_mag');
            $table->string('long_mag');
            $table->string('lat_mag');
            $table->string('tel_mag');
            $table->string('mail_mag');
            $table->string('siret_mag');
            $table->string('photo1_mag');
            
            $table->integer('id_ville')->unsigned();
            $table->foreign('id_ville')->references('id')->on('villes');
            
            $table->integer('id_type')->unsigned();
            $table->foreign('id_type')->references('id')->on('types');     
            
            $table->integer('id_cat')->unsigned();
            $table->foreign('id_cat')->references('id')->on('categories');
            
            $table->integer('id_resp')->unsigned();
            $table->foreign('id_resp')->references('id')->on('users');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('magasins');
    }
}
