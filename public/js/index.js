$(document).ready(function() {
    const checkClassForm = $("form").hasClass("needs-validation").toString();
    const checkConfPassword = $(':password').is('#password_confirm');
    const checkLogin = $('input').is('#email');

    if (checkClassForm) {
        window.addEventListener('load', function() {
            const forms = document.getElementsByClassName('needs-validation');
            const validation = Array.prototype.filter.call(forms, function(form) {
                form.addEventListener('submit', function(event) {
                    if (form.checkValidity() === false) {
                        event.preventDefault();
                        event.stopPropagation();
                    }
                    form.classList.add('was-validated');
                }, false);
            });
        }, false);

        if (checkConfPassword) {
            showPwd.addEventListener("click", togglePassword, false);
            showPwdConf.addEventListener("click", togglePasswordConf, false);
        } else if (checkLogin) {
            showPwd.addEventListener("click", togglePassword, false);
        }
    }
});

function togglePassword() {
    if (password.type === "password") {
        password.type = "text";
        iconPwd.className = "fas fa-eye-slash";
    } else {
        password.type = "password";
        iconPwd.className = "fas fa-eye"
    }
}

function togglePasswordConf() {
    if (password_confirm.type === "password") {
        password_confirm.type = "text";
        iconPwdConf.className = "fas fa-eye-slash";
    } else {
        password_confirm.type = "password";
        iconPwdConf.className = "fas fa-eye"
    }
}
