$(document).ready(function() {
    /* VARIABLES */
    /*Pour leaflet*/
    let map = L.map('map');

    let osmUrl='http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
    let osmAttrib='© OpenStreetMap';
    let osm = new L.TileLayer(osmUrl, {attribution: osmAttrib});

    map.setView([47.0, 3.0], 6);
    map.addLayer(osm);
    map.scrollWheelZoom.disable();

    let input = document.getElementById('villes');
    let datalist = document.getElementById('villeDatalist');

    /*Stockage de la requête sql précédente*/
    let memoryReq;

    /* AJAX */
    $post=(url,data,done,error)=>{
        let getUrl=(objet)=>{
            var result = new Array();
            for(var i in objet){
                result.push(i+"="+encodeURIComponent(objet[i]));
            }
            return result.join('&');
        };

        let Xhr=()=>{
            let xhr = null;
            if (window.XDomainRequest) {
                xhr = new XDomainRequest();
            } else if (window.XMLHttpRequest) {
                xhr = new XMLHttpRequest();
            } else {
                alert("Votre navigateur ne gère pas l'AJAX cross-domain !");
            }
            return xhr;
        }

        xhttp =  Xhr();

        xhttp.onload=function(){
            if (this.status==200) done(this)
            else error(this)
        }

        data = getUrl(data)+"& cache="+new Date().getTime();
        xhttp.open("post", url, true);
        xhttp.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
        xhttp.send(data);
    }
    let error = () =>{return alert('Il semble qu\'il y ait un problème')}

    let findCityDone = (r) =>{
        let rep = JSON.parse(r.responseText);

        let nameCity = input.value.split(';');
        /*console.log('nameCity : '+nameCity);
        console.log(nameCity[1]);
        console.log(typeof nameCity[1]);*/
        console.log(memoryReq);
        /*console.log(memoryReq[1]);
        console.log(typeof memoryReq[1]);*/
        console.log('rep : '+rep);

        /*On suprime le contenu de la datalist*/
        datalist.innerHTML="";

        /*On actualise memoryReq que si rep retourne un résultat*/
        if (rep.length !== 0) {memoryReq = rep;}

        /*Si la requête sql (rep) est vide, on vérifie dans memoryReq (qui est le stockage de la requête sql précédente) sinon on rempli la datalist avec la rep*/
        if(rep.length == 0){
            for (let i=0; i<memoryReq.length; i++){
                if ((nameCity[0].toUpperCase() == memoryReq[i].nom_ville.toUpperCase()) && (nameCity[1] == memoryReq[i].cp_ville)) {
                    map.setView([memoryReq[i].lat_ville, memoryReq[i].long_ville], 15);
                    /*On appelle une requête ajax pour afficher les magasins*/
                    /*findStructure();*/
                }
            }
        } else {
            for (let i=0; i<rep.length; i++){
                /*Si le nom dans l'input correspond au nom d'une ville, on recentre la carte sur la ville*/
                if (nameCity[0].toUpperCase() == rep[i].nom_ville.toUpperCase()) {
                    map.setView([rep[i].lat_ville, rep[i].long_ville], 15);
                    /*Requête ajax pour afficher les magasins*/
                    /*findStructure();*/
                }

                let option = datalist.appendChild(document.createElement('option'));
                option.setAttribute('value', rep[i].nom_ville+';'+rep[i].cp_ville);
            }
        }
    };


    /* FONCTIONS */


    /* Lance la requête ajax pour rechercher les villes */
    // function findCity(inputValue){
    //     $post('{{route('city')}}', {inputValue:inputValue, _token: '{{ csrf_token() }}'}, findCityDone, error);
    // }

    /*Teste la valeur de l'input et lance la requête ajax*/
    function startFindCity(){
        if(input.value.length > 2){
            findCity(input.value);
        } else if (datalist.childElementCount) {
            datalist.innerHTML="";
        }
    }

    /*Code à exécuter*/
    document.addEventListener('DOMContentLoaded', function(){
        /*On ajoute un évènement startFindCity sur l'input quand on tape sur le clavier*/
        input.addEventListener('keyup', function(){startFindCity();});
        /*On ajoute le même évènement sur la datalist au clic*/
        datalist.addEventListener('click', function(){startFindCity();});

        /*On ajoute le même évènement sur la datalist quand on appuie sur une touche*/
        datalist.addEventListener('keyup', function(){startFindCity();});
    });
});
