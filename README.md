## Environnement n�cessaire pour lancer le projet
- NodeJS (Facultatif)
- Serveur local
- Composer
- PHP >= 7.0.0
- Configurer une alias (Si vous utilisez WampServer "D:\dossier\wamp64\www\dismap\public")
- Une base de donn�es (MySql)
- OpenSSL PHP Extension
- PDO PHP Extension
- Mbstring PHP Extension
- Tokenizer PHP Extension
- XML PHP Extension

## D�marrage

- Lancer la commande "composer install" � la racine pour installer les d�pendances
- Cr�er la base de donn�es avec ce nom "db_project" (si n�cessaire modifier les identifiants dans le .env
- Puis lancer la commande "php artisan migrate" � la racine du projet
- Rendez-vous � l'adresse de votre alias (Exemple: dismap/)
- Et on vous souhaite une bonne navigation :)

## Dev Front

- Pour le d�veloppemet Front aller dans public > lancer la commande "npm install"
