<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Magasin extends Model
{
    //protected $table = 'mes_magasins';

    protected $fillable = [
        'nom_mag',
        'ad1_mag',
        'ad2_mag',
        'long_mag',
        'lat_mag',
        'tel_mag',
        'mail_mag',
        'siret_mag',
        'photo1_mag',
        'tel_mag',
        'id_ville',
        'id_type',
        'id_cat',
        'id_resp'
     ];
    // protected $guarded = ['_token'];
}
