<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Adhesion extends Model
{
    protected $fillable = [
        'id_user',
        'id_promo',
        'id_avis',
        'note_avis',
        'commentaire_avis'
    ];
}
