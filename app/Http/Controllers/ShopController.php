<?php

namespace App\Http\Controllers;

use App\Promotion;
use App\User;
use App\Magasin;
use App\Ville;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class ShopController extends Controller
{

    public function __construct() {
        $this->middleware('auth');
        $this->middleware('resp');
    }

    public function getShops() {
        $user = User::findOrFail(Auth::id());
        $shops = DB::table('magasins')->where('id_resp', $user->id)->get();
        $types = DB::table('types')->get();
        $categories = DB::table('categories')->get();

        return view('shop.resp_shops', [
            'user' => $user,
            'shops' => $shops,
            'types' => $types,
            'categories' => $categories,
        ]);
    }

    public function addShopPost(Request $request) {
        $this->validate($request, [
            'nom_mag' => 'required|max:50',
            'ad1_mag' => 'required|max:50',
            'ad2_mag' => 'max:50',
            'tel_mag' => 'regex:/[0-9]{10}/',
            'mail_mag' => 'required|email|max:50',
            'siret_mag' => 'required|max:14|regex:/[0-9]{14}/',
            'photo1_mag' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'id_ville' => 'required|regex:/.&./',
            'id_type' => 'required',
            'id_cat' => 'required',
        ]);

        $data = $request->all();
        $searchVille = explode(" & ", $data['id_ville']);

        $data['id_ville'] = DB::table('villes')
            ->where('nom_ville', $searchVille[0])
            ->where('cp_ville', $searchVille[1])
            ->value('id');
        $ville = Ville::findOrFail($data['id_ville'])['nom_ville'];
        $adresse = $data['ad1_mag'] . ' ' . $ville;
        /* API locationIQ */

        $token = 'eb524926dace73';
        $curl = curl_init('https://eu1.locationiq.com/v1/search.php?key=' . $token . '&q=' . $adresse . '&format=json');

        curl_setopt_array($curl, array(
        CURLOPT_RETURNTRANSFER    =>  true,
        CURLOPT_FOLLOWLOCATION    =>  true,
        CURLOPT_MAXREDIRS         =>  10,
        CURLOPT_TIMEOUT           =>  30,
        CURLOPT_CUSTOMREQUEST     =>  'GET',
        ));

        $response = curl_exec($curl);
        curl_close($curl);

        if ($response) {
            $location = json_decode($response)[0];

            $data['lat_mag'] = $location->lat;
            $data['long_mag'] = $location->lon;

            $data['id_resp'] = Auth::id();

            $data['photo1_mag'] = $data['nom_mag'] . time() . '.' . request()->photo1_mag->getClientOriginalExtension();
            request()->photo1_mag->move(public_path('uploads/magasins'), $data['photo1_mag']);

            Magasin::create($data);

            return redirect()->route('resp_shops')->with('messageMag', 'Magasin ajouté !');
        } else {
            return redirect()->route('resp_shops')->with('messageMagErr', 'Une erreur est survenue !');
        }

    }

    public function shop($id_shop) {
        $shop = Magasin::findOrFail($id_shop);
        if (Auth::id() == $shop->id_resp ) {
            $user = User::findOrFail(Auth::id());
            $types = DB::table('types')->get();
            $shop_ville = DB::table('magasins')->join('villes', 'magasins.id_ville', '=', 'villes.id')->where('magasins.id_ville', $shop->id_ville)->get();
            $categories = DB::table('categories')->get();
            $promotions = DB::table('promotions')->where('id_mag', $id_shop)->get();
            return view('shop.shop', [
                'shop' => Magasin::findOrFail($id_shop),
                'promotions' => $promotions,
                'user' => $user,
                'shop_ville' => $shop_ville,
                'types' => $types,
                'categories' => $categories
            ]);
        }
        return redirect('/resp/shops');
    }

    public function editShop($id_shop) {
        $shop = Magasin::findOrFail($id_shop);
        $user = User::findOrFail(Auth::id());
        $types = DB::table('types')->get();
        $categories = DB::table('categories')->get();
        return view('shop.shop', [
            'shop' => $shop,
            'user' => $user,
            'types' => $types,
            'categories' => $categories,
        ]);
    }

    public function editShopPost(Request $request, $id_shop) {
        $this->validate($request, [
            'nom_mag' => 'required|max:50',
            'ad1_mag' => 'required|max:50',
            'ad2_mag' => 'max:50',
            'tel_mag' => 'regex:/[0-9]{10}/',
            'mail_mag' => 'required|email|max:50',
            'siret_mag' => 'required|max:14|regex:/[0-9]{14}/',
            'photo1_mag' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'id_ville' => 'required|regex:/.&./',
            'id_type' => 'required',
            'id_cat' => 'required',
        ]);
        $shop = Magasin::findOrFail($id_shop);
        $searchVille = explode(" & ", $request->id_ville);
        $id_ville = DB::table('villes')
            ->where('nom_ville', $searchVille[0])
            ->where('cp_ville', $searchVille[1])
            ->value('id');
        $ville = Ville::findOrFail($id_ville)['nom_ville'];
        $adresse = Input::get('ad1_mag') . ' ' . $ville;
        /* API locationIQ */

        $token = 'eb524926dace73';
        $curl = curl_init('https://eu1.locationiq.com/v1/search.php?key=' . $token . '&q=' . $adresse . '&format=json');

        curl_setopt_array($curl, array(
        CURLOPT_RETURNTRANSFER    =>  true,
        CURLOPT_FOLLOWLOCATION    =>  true,
        CURLOPT_MAXREDIRS         =>  10,
        CURLOPT_TIMEOUT           =>  30,
        CURLOPT_CUSTOMREQUEST     =>  'GET',
        ));

        $response = curl_exec($curl);
        curl_close($curl);

        if($response) {
            $location = json_decode($response)[0];
            if (isset(request()->photo1_mag)) {
                $photo1_mag = $shop->nom_mag . time() . '.' . request()->photo1_mag->getClientOriginalExtension();
                request()->photo1_mag->move(public_path('uploads/magasins'), $photo1_mag);
                $shop->photo1_mag = $photo1_mag;
            }

            $shop->nom_mag = Input::get('nom_mag');
            $shop->ad1_mag = Input::get('ad1_mag');
            $shop->ad2_mag = Input::get('ad2_mag');
            $shop->long_mag = $location->lon;
            $shop->lat_mag = $location->lat;
            $shop->tel_mag = Input::get('tel_mag');
            $shop->mail_mag = Input::get('mail_mag');
            $shop->siret_mag = Input::get('siret_mag');
            $shop->tel_mag = Input::get('tel_mag');
            $shop->id_ville = $id_ville;
            $shop->id_type = Input::get('id_type');
            $shop->id_cat = Input::get('id_cat');

            $shop->save();

            return redirect()->route('shop', [
                'shop' => $shop,
            ])->with('messageMag', 'Magasin modifié');

        } else {
            return redirect()->route('shop', [
                'shop' => $shop,
            ])->with('messageMagErr', 'Une erreur est survenue !');
        }
    }
}
