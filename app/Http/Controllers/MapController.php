<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MapController extends Controller
{
    public function getCities(Request $request){

    	$inputValue = $request->input('inputValue');
    	$resultCities = DB::table('villes')->where('nom_ville', 'like', $inputValue.'%')->limit(10)->get();

    	/*$cats = Category::get();
    	return view('dfsdf', ['cats' => $cats->toJson])

    	var cats = {!! $cats->toJson() !!}*/

        $result = [
            'cities' => $resultCities
        ];
    	return $result;
    }

    public function getTypesCategories(){
        $resultTypes = DB::table('types')->get();
        $resultCategories = DB::table('categories')->get();
        $result = [
            'types' => $resultTypes,
            'categories' => $resultCategories
        ];
        return $result;
    }

    public function getShopWithPromoActives(Request $request)
    {

        $latMapMin = $request->input('latMapMin');
        $latMapMax = $request->input('latMapMax');
        $longMapMin = $request->input('longMapMin');
        $longMapMax = $request->input('longMapMax');

        $idType = $request->input('idType');
        $idCat = $request->input('idCategorie');

        if ($idType) {
            $resultPromo = DB::table('magasins')
                ->join('promotions', 'magasins.id', '=', 'promotions.id_mag')
                ->whereBetween('magasins.lat_mag', [$latMapMin, $latMapMax])
                ->whereBetween('magasins.long_mag', [$longMapMin, $longMapMax])
                ->where('promotions.etat_promo', '=', '1')
                ->where('magasins.id_type', '=', $idType)
                ->get();
        } else if($idCat){
            $resultPromo = DB::table('magasins')
                ->join('promotions', 'magasins.id', '=', 'promotions.id_mag')
                ->whereBetween('magasins.lat_mag', [$latMapMin, $latMapMax])
                ->whereBetween('magasins.long_mag', [$longMapMin, $longMapMax])
                ->where('promotions.etat_promo', '=', '1')
                ->where('magasins.id_type', '=', $idType)
                ->where('magasins.id_cat', '=', $idCat)
                ->get();
        } else {
            $resultPromo = DB::table('magasins')
                ->join('promotions', 'magasins.id', '=', 'promotions.id_mag')
                ->whereBetween('magasins.lat_mag', [$latMapMin, $latMapMax])
                ->whereBetween('magasins.long_mag', [$longMapMin, $longMapMax])
                ->where('promotions.etat_promo', '=', '1')
                ->get();
        }
        
		return $resultPromo;
    }
}
