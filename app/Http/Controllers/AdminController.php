<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Type;
use App\Categorie;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

class AdminController extends Controller
{
    public function dashboard() 
    {
        /* Affiche la liste des données éditables, pour exemple : types et catégories */
        $user = User::findOrFail(Auth::id());
        $types = DB::table('types')->get();
        $cats = DB::table('categories')->get();
        if($user->type === 'admin') {
            // dd($types, $cats);
            return view('dashboard.admin.admin',  [
                'types' => $types,
                'cats' => $cats
            ]);
        } else {
            return redirect()->action('AdhesionController@getAdhesion');
        }
    }

    public function addTypePost(Request $request) 
    {
        // dd($request);
        $this->validate($request, [
            'libelle_type' => 'required|max:50'
        ]);

        $data = $request->all();
        Type::create($data);
        return redirect()->route('admin')->with('messageType', 'Type ajouté');

    }

    public function displayType($id_type) 
    {
        $type = Type::findOrFail($id_type);
        $cats = DB::table('categories')->where('id_type', $id_type)->get();

        return view('dashboard.admin.type', [
            'type' => $type,
            'cats' => $cats
        ]);
    }

    public function addCatPost(Request $request, $id_type) 
    {
        // dd($request);
        $this->validate($request, [
            'libelle_cat' => 'required|max:50',
            'id_type' => 'required'
        ]);

        $data = $request->all();
        Categorie::create($data);
        return redirect()->route('display_type', ['id_type' => $id_type])->with('messageCat', 'Catégorie ajouté');

    }

    public function editType($id_type)
    {
        $type = Type::findOrFail($id_type);
        return view('dashboard.admin.type_edit', [
            'type' => $type
        ]);
    }

    public function editTypePost(Request $request, $id_type)
    {
        $this->validate($request, [
            'libelle_type' => 'required|max:50',
        ]);
        $type = Type::findOrFail($id_type);
        $type->libelle_type = Input::get('libelle_type');

        $type->save();

        return redirect()->route('edit_type', [
            'id_type' => $type->id
        ])->with('messageType', 'Type modifié');
    }
}
