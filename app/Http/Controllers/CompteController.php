<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class CompteController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }

    public function getEdit() {
        $user = User::findOrFail(Auth::id());

        return view('dashboard.user_edit', ['user'=>$user]);
    }

    public function postEdit(Request $request) {

        $this->validate($request, [
            'firstname' => 'required|max:50',
            'lastname' => 'required|max:50',
            'email' => 'required|email',
            'phone' => 'nullable|size:10|regex:/[0-9]{10}/',
            'password' => 'required_with:password_confirmation|confirmed'
        ]);

        $user = User::findOrFail($request->input('user_id'));
        $user->firstname = $request->input('firstname');
        $user->lastname = $request->input('lastname');
        $user->email = $request->input('email');
        $user->phone = $request->input('phone');

        $user->save();

        return redirect()->route('user_edit');
    }

    public function passwordEdit(Request $request) {

        $user = User::findOrFail(Auth::id());

        $current_password = $user->password;

        if(Hash::check($request->current_password, $current_password))
        {
            $this->validate($request, ['password' => 'required_with:password_confirmation|confirmed|min:6']);
            $user->password = Hash::make($request->input('password'));
            $user->save();
        }
        else {
            $this->validate($request, ['current_password'=>'confirmed',
                'password' => 'required_with:password_confirmation|confirmed|min:6']);
        }

        return redirect()->route('user_edit');
    }
}
