<?php

namespace App\Http\Controllers;

use App\Magasin;
use App\Promotion;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DateTime;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;

class PromoController extends Controller
{

    public function __construct() {
        $this->middleware('auth');
    }

    public function addPromoPost(Request $request) {
        $this->validate($request, [
            'libelle_promo' => 'required|max:50',
            'date_debut_promo' => 'required',
            'date_fin_promo' => 'required',
            'photo1_promo' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048',
            'photo2_promo' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'photo3_promo' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048']);

        $id_mag = $request->input('id_mag');

        $data = $request->all();

        $request->etat_promo ? $data['etat_promo'] = "1" : $data['etat_promo'] = "0";

        $data['photo1_promo'] = "photo1-" . time() . '.' . request()->photo1_promo->getClientOriginalExtension();
        request()->photo1_promo->move(public_path('uploads/promotions'), $data['photo1_promo']);

        if (isset(request()->photo2_promo)) {
            $data['photo2_promo'] = "photo2-" . time() . '.' . request()->photo2_promo->getClientOriginalExtension();
            request()->photo2_promo->move(public_path('uploads/promotions'), $data['photo2_promo']);
        }

        if (isset(request()->photo3_promo)) {
            $data['photo3_promo'] = "photo3-" . time() . '.' . request()->photo3_promo->getClientOriginalExtension();
            request()->photo3_promo->move(public_path('uploads/promotions'), $data['photo3_promo']);
        }

        $data['code_promo']= str_pad(rand(0, pow(10, 3)-1), 3, '0', STR_PAD_LEFT);
        $data['code_avis_promo']= str_pad(rand(0, pow(10, 3)-1), 3, '0', STR_PAD_LEFT);

        Promotion::create($data);

        return redirect()->route('shop', [$id_mag])->with('messagePromo', 'Promotion ajoutée');
    }


    public function getPromo($id_promo) {

        $promo = Promotion::findOrFail($id_promo);
        $shop = Magasin::findOrFail($promo->id_mag);
        $commentaires = DB::table('adhesions')
            ->where('id_promo', $id_promo)
            ->join('users', 'users.id', '=', 'adhesions.id_user')
            ->get();

        $datetime = new DateTime();
        $datetime->format('Y-m-d H:i:s');

        $datefin = new DateTime($promo->date_fin_promo);

        $temps = date_diff($datetime, $datefin);
        if (count($commentaires)>0) {
            return view('promotion.promotion', [
                'promo' => $promo,
                'shop' => $shop,
                'temps' => $temps,
                'commentaires' => $commentaires,
            ]);
        } else {
            return view('promotion.promotion', [
                'promo' => $promo,
                'shop' => $shop,
                'temps' => $temps,
            ]);
        }
    }

    public function editPromo($id_promo) {
        $promo = Promotion::findOrFail($id_promo);
        $shop = Magasin::findorFail($promo->id_mag);
        if (Auth::id() == $shop->id_resp ) {
            $promo = Promotion::findOrFail($id_promo);
            $shop = Magasin::findOrFail($promo->id_mag);

            return view('promotion.promotion_edit', [
                'promo' => $promo,
                'shop' => $shop,
            ]);
        }
        return redirect()->route('shop', [$shop->id]);
    }

    public function editPromoPost(Request $request, $id_promo) {
        $this->validate($request, [
            'libelle_promo' => 'required|max:50',
            'photo1_promo' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'photo2_promo' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'photo3_promo' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',]);

        $promo = Promotion::findOrFail($id_promo);

        $request->etat_promo ? $promo->etat_promo = "1" : $promo->etat_promo = "0";

        $promo->libelle_promo = $request->libelle_promo;

        if (isset(request()->photo1_promo)) {
            $photo1_promo = "photo1-" . time() . '.' . request()->photo1_promo->getClientOriginalExtension();
            request()->photo1_promo->move(public_path('uploads/promotions'), $photo1_promo);
            $promo->photo1_promo = $photo1_promo;
        }

        if (isset(request()->photo2_promo)) {
            $photo2_promo = "photo2-" . time() . '.' . request()->photo2_promo->getClientOriginalExtension();
            request()->photo2_promo->move(public_path('uploads/promotions'), $photo2_promo);
            $promo->photo2_promo = $photo2_promo;
        }

        if (isset(request()->photo3_promo)) {
            $photo3_promo = "photo3-" . time() . '.' . request()->photo3_promo->getClientOriginalExtension();
            request()->photo3_promo->move(public_path('uploads/promotions'), $photo3_promo);
            $promo->photo3_promo = $photo3_promo;
        }

        $promo->save();

        return redirect()->route('edit_promo', [$promo->id])->with('message', 'Promotion modifiée');
    }

}
