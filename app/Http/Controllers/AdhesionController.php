<?php

namespace App\Http\Controllers;

use App\Adhesion;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AdhesionController extends Controller
{
    public function adhesion($id_promo) {

        $data['id_promo'] = $id_promo;
        $data['id_user'] = Auth::id();

        Adhesion::updateOrCreate($data);

        return redirect()->route('get_promo', [$id_promo])->with('message', 'Promotion sauvegardée');
    }

    public function getAdhesion () {

        $promos = DB::table('promotions')
            ->join('adhesions', 'promotions.id', '=', 'adhesions.id_promo')
            ->where('adhesions.id_user', Auth::id())
            ->get();

        $user = User::findOrFail(Auth::id());
        if($user->type === 'admin') {
            return redirect()->action('AdminController@dashboard');
        } else {
            return view('dashboard.home', [
                'promos' => $promos,
            ]);
        }
    }

    public function deleteAdhesion ($id_adhesion) {

        Adhesion::destroy($id_adhesion);

        return redirect()->route('adhesion_list');
    }

    public function commentaire_post($id_promo)
    {
        $code_avis_promo = DB::table('promotions')->where('id', $id_promo)->value('code_avis_promo');
        $data = request()->except(['_token']);
        if ($data['code_avis_promo'] !== $code_avis_promo) {
            return redirect()->route('get_promo', [$id_promo])->with('messageCodeAvis', 'Code Avis non conforme, veuillez réessayer');
        } else {
            unset($data['code_avis_promo']);
            $data['id_promo'] = $id_promo;
            $data['id_user'] = Auth::id();
            $id_adhesion = DB::table('adhesions')
                ->where('id_user', $data['id_user'])
                ->where('id_promo', $data['id_promo'])
                ->value('id');
            $adhesion = Adhesion::findOrFail($id_adhesion);
            $adhesion->commentaire_avis = $data['commentaire_avis'];
            $adhesion->note_avis = $data['note_avis'];
            $adhesion->save();

            return redirect()->route('get_promo', [$id_promo])->with('messageAvis', 'Votre commentaire a bien été envoyé.');
        }
    }
}
