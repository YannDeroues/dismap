<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Promotion extends Model
{
    protected $fillable = [
        'libelle_promo',
        'date_debut_promo',
        'date_fin_promo',
        'photo1_promo',
        'photo2_promo',
        'photo3_promo',
        'etat_promo',
        'code_promo',
        'code_avis_promo',
        'id_mag'
    ];
}
